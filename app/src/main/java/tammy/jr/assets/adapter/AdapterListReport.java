package tammy.jr.assets.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Report;
import tammy.jr.home.Report.UbahReportActivity;
import tammy.jr.home.ReportFragment;

public class AdapterListReport extends RecyclerView.Adapter<AdapterListReport.MyViewHolder> {
    Context context;
    List<Report> reportList;
    PrefManager prefManager;
    ReportFragment fragment;
    ApiServices apiServices = ApiUtils.getApiServices();

    public AdapterListReport(Context context, List<Report> reportList,ReportFragment fragment) {
        this.context = context;
        this.reportList = reportList;
        this.fragment = fragment;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_report, parent, false));
    }

    @SuppressLint({"SetTextI18n", "RestrictedApi", "NonConstantResourceId"})
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final Report data = reportList.get(position);
        if (prefManager.getLevel().equals(AppData.ADMIN)){
            holder.llNik.setVisibility(View.VISIBLE);
            holder.tvNik.setText(""+data.getNik());
        }else{
            holder.llNik.setVisibility(View.GONE);
        }
        holder.tvHariTgl.setText(data.getHari_tgl_val());
        holder.tvNama.setText(data.getNamaKaryawan());
        holder.tvProject.setText(data.getProject());
        holder.tvUraian.setText(data.getUraian());
        holder.tvOther.setText(data.getLain());
        holder.tvTglPengerjaan.setText(data.getWaktu_val()+ " - "+data.getTgl_selesai_val());
        holder.tvKeterangan.setText(data.getKuan());

        holder.btnMenu.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(context,holder.btnMenu);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.menu_update:
                        Intent intent = new Intent(context, UbahReportActivity.class);
                        intent.putExtra("data",data);
                        fragment.startActivityForResult(intent,123);
                        break;
                    case R.id.menu_delete:
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle(null);
                        alertDialog.setMessage("Apakah anda yakin untuk hapus report ini?");
                        alertDialog.setCancelable(true);
                        alertDialog.setPositiveButton("Hapus", (dialog, which) -> apiServices.deleteDataReport(data.getId()).enqueue(new Callback<ResponMessage>() {
                            @Override
                            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                                if (response.isSuccessful()){
                                    assert response.body() != null;
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    fragment.loadData();
                                }else{
                                    Toast.makeText(context, "gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                                t.printStackTrace();
                            }
                        }));
                        alertDialog.setNegativeButton("Batal", (dialog, which) -> dialog.cancel());
                        alertDialog.show();
                        break;
                }
                return false;
            });
            popupMenu.inflate(R.menu.adapter_report_menu);
            popupMenu.getMenu().findItem(R.id.menu_delete).setVisible(false);

            switch (prefManager.getLevel()) {
                case AppData.KARYAWAN:
                    holder.lytKonfirmasi.setVisibility(View.GONE);
                    popupMenu.getMenu().findItem(R.id.menu_update).setVisible(true);
                    break;
                case AppData.LEADER:
                case AppData.ADMIN:
                    holder.lytKonfirmasi.setVisibility(View.VISIBLE);
                    holder.btnMenu.setVisibility(View.GONE);
                    break;
                default:
                    holder.lytKonfirmasi.setVisibility(View.GONE);
                    holder.btnMenu.setVisibility(View.GONE);
                    break;
            }

            if (data.getApproval()!=null){
                holder.btnMenu.setVisibility(View.GONE);
            }

            try{
                Field field = popupMenu.getClass().getDeclaredField("popup_report");
                field.setAccessible(true);
                MenuPopupHelper popupHelper = (MenuPopupHelper) field.get(popupMenu);
                assert popupHelper != null;
                popupHelper.setForceShowIcon(true);
            }catch (Exception ignored){}

            popupMenu.show();
        });

        switch (prefManager.getLevel()) {
            case AppData.KARYAWAN:
                holder.lytKonfirmasi.setVisibility(View.GONE);
                break;
            case AppData.LEADER:
            case AppData.ADMIN:
                holder.lytKonfirmasi.setVisibility(View.VISIBLE);
                holder.btnMenu.setVisibility(View.GONE);
                break;
            default:
                holder.lytKonfirmasi.setVisibility(View.GONE);
                holder.btnMenu.setVisibility(View.GONE);
                break;
        }

        if (data.getApproval()!=null){
            holder.btnMenu.setVisibility(View.GONE);
        }

        holder.btnApproved.setOnClickListener(v -> {
            fragment.postKonfirmasi(data.getId(),"1");
            fragment.loadData();
        });
        holder.btnNotApproved.setOnClickListener(v -> {
            fragment.postKonfirmasi(data.getId(),"0");
            fragment.loadData();
        });

        if (TextUtils.isEmpty(data.getApproval())){
            holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.menunggu));
            holder.tvStatus.setText("Menunggu Approval");
        }else{
            if (data.getApproval().equals("0")){
                holder.lytKonfirmasi.setVisibility(View.GONE);
                holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.ditolak));
                holder.tvStatus.setText("Ditolak Oleh "+data.getNamaLeader());
            }else if (data.getApproval().equals("1")){
                holder.lytKonfirmasi.setVisibility(View.GONE);
                holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.diterima));
                holder.tvStatus.setText("Disetujui Oleh "+data.getNamaLeader());
            }
        }
    }

    @Override
    public int getItemCount() {
        return (reportList!=null)?reportList.size():0;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvId,tvHariTgl,tvProject,tvUraian,tvOther,tvTglPengerjaan,tvNik,tvKeterangan,tvStatus,tvNama;
        LinearLayout llProject,llUraian,llOther,llTglPengerjaan,llNik,llKeterangan;
        ImageView btnMenu;
        LinearLayout lytKonfirmasi;
        Button btnApproved,btnNotApproved;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tv_id);
            tvNik = itemView.findViewById(R.id.tv_nik);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvHariTgl = itemView.findViewById(R.id.tv_haritgl);
            tvProject = itemView.findViewById(R.id.tv_project);
            tvUraian = itemView.findViewById(R.id.tv_uraian);
            tvOther = itemView.findViewById(R.id.tv_other);
            tvTglPengerjaan =itemView.findViewById(R.id.tv_tglPengerjaan);
            tvKeterangan = itemView.findViewById(R.id.tv_ket);
            llNik = itemView.findViewById(R.id.ll_nik);
            llProject = itemView.findViewById(R.id.ll_project);
            llUraian = itemView.findViewById(R.id.ll_uraian);
            llOther = itemView.findViewById(R.id.ll_other);
            llTglPengerjaan =itemView.findViewById(R.id.ll_tglPengerjaan);
            llKeterangan = itemView.findViewById(R.id.ll_ket);
            btnMenu = itemView.findViewById(R.id.btnMenu);
            tvStatus = itemView.findViewById(R.id.tv_status);
            lytKonfirmasi = itemView.findViewById(R.id.layout_konfirmasi);
            btnApproved = itemView.findViewById(R.id.btn_approved);
            btnNotApproved = itemView.findViewById(R.id.btn_not_approved);
        }
    }


}
