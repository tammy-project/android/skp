package tammy.jr.home;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponAuth;
import tammy.jr.assets.model.User;
import tammy.jr.home.Profil.GantiPasswordActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilFragment extends Fragment {
    private TextView tvNik,tvNama,tvBagian,tvJabatan,tvDivisi,tvLevel;
    private CircleImageView imgProfil;
    private Button btnUbahpassword;
    private PrefManager prefManager;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private NestedScrollView baseLayout;
    private ProgressBar loadingLayout;
    private View rootView;
    private Context context;

    public ProfilFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profil, container, false);
        initComponent();
        return rootView;
    }

    private void initComponent() {
        loadingLayout = rootView.findViewById(R.id.loading_layout);
        baseLayout = rootView.findViewById(R.id.base_layout);
        tvNik = rootView.findViewById(R.id.tv_nik);
        tvNama = rootView.findViewById(R.id.tv_nama);
        tvBagian = rootView.findViewById(R.id.tv_bagian);
        tvJabatan = rootView.findViewById(R.id.tv_jabatan);
        tvDivisi = rootView.findViewById(R.id.tv_divisi);
        tvLevel = rootView.findViewById(R.id.tv_level);
        imgProfil = rootView.findViewById(R.id.img_profil);
        btnUbahpassword = rootView.findViewById(R.id.btn_ubahpassword);
        prefManager = new PrefManager(context);

        btnUbahpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, GantiPasswordActivity.class));
            }
        });

        loadData();
    }

    private void loadData() {
        loadingStarted();
        apiServices.getUserData(prefManager.getNIK()).enqueue(new Callback<ResponAuth>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NotNull Call<ResponAuth> call, @NotNull Response<ResponAuth> response) {
                if (response.isSuccessful()){
                    try{
                        assert response.body() != null;
                        if (response.body().getStatus()){
                            User data = response.body().getResult();
                            if (!TextUtils.isEmpty(data.getFotoUrl())){
                                Picasso.get()
                                        .load(AppData.URL_FOTO_KARYAWAN+data.getFotoUrl())
                                        .fit()
                                        .error(R.drawable.no_image_avaible)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(imgProfil);
                            }
                            tvNik.setText(""+data.getNik());
                            tvNama.setText(""+data.getNama());
                            tvBagian.setText(""+data.getBagian());
                            tvJabatan.setText(""+data.getJabatan());
                            tvDivisi.setText(""+data.getDivisi());
                            tvLevel.setText(""+data.getLevel());
                            loadingFinished();
                        }else{
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponAuth> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void loadingStarted(){
        loadingLayout.setVisibility(View.VISIBLE);
        baseLayout.setVisibility(View.GONE);
    }

    private void loadingFinished(){
        loadingLayout.setVisibility(View.GONE);
        baseLayout.setVisibility(View.VISIBLE);
    }

}
