package tammy.jr.home.Profil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;

public class GantiPasswordActivity extends AppCompatActivity {
    private Context context;
    private EditText edtPassLama,edtPassBaru,edtUlangiPass;
    private Button btnSimpan;
    private PrefManager prefManager;
    private ProgressDialog loading;
    private ApiServices apiServices = ApiUtils.getApiServices();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti_password);
        context = this;
        initComponent();
    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        edtPassLama = findViewById(R.id.edt_pass_lama);
        edtPassBaru = findViewById(R.id.edt_pass_baru);
        edtUlangiPass = findViewById(R.id.edt_ulangi_pass);
        btnSimpan = findViewById(R.id.btn_simpan);

        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Password");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPassLama.getText().toString().isEmpty()){
                    Toast.makeText(context, "Password lama harus di isi", Toast.LENGTH_SHORT).show();
                }else if (edtPassBaru.getText().toString().isEmpty()){
                    Toast.makeText(context, "Password baru harus di isi", Toast.LENGTH_SHORT).show();
                }else if (edtUlangiPass.getText().toString().isEmpty()){
                    Toast.makeText(context, "ulangi password baru", Toast.LENGTH_SHORT).show();
                }else if (!edtUlangiPass.getText().toString().equals(edtPassBaru.getText().toString())){
                    Toast.makeText(context, "password baru anda tidak sesuai", Toast.LENGTH_SHORT).show();
                }else{
                    gantiprocess();
                }
            }
        });
    }

    private void gantiprocess() {
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.updatePassword(prefManager.getNIK(),edtPassBaru.getText().toString(),edtPassLama.getText().toString()).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
