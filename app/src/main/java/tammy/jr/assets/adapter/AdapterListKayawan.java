package tammy.jr.assets.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.User;
import tammy.jr.home.Karyawan.EditKaryawanActivity;

public class AdapterListKayawan extends RecyclerView.Adapter<AdapterListKayawan.MyViewHolder> implements Filterable {
    private Context context;
    private List<User> userList;
    private List<User> userFilteredList;
    private Fragment fragment;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private PrefManager prefManager;

    public AdapterListKayawan(Context context, List<User> userList,Fragment fragment) {
        this.context = context;
        this.userList = userList;
        this.fragment = fragment;
        this.userFilteredList = userList;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_karyawan, parent, false));
    }

    @SuppressLint({"SetTextI18n", "RestrictedApi", "NonConstantResourceId"})
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        try{
            final User data = userFilteredList.get(position);
            Picasso.get()
                    .load(AppData.URL_FOTO_KARYAWAN+data.getFotoUrl())
                    .fit()
                    .error(R.drawable.no_image_avaible)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.imgKaryawan);
            holder.tvNik.setText("NIK :"+data.getNik());
            holder.tvNama.setText("Nama :"+data.getNama());
            holder.tvBagian.setText("Bagian :"+data.getBagian());
            holder.tvJabatan.setText("Jabatan :"+data.getJabatan());
            holder.tvDivisi.setText("Divisi :"+data.getDivisi());
            holder.tvLevel.setText("Level "+data.getLevel());

            //set menu
            holder.btnMenu.setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context,holder.btnMenu);
                popupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()){
                        case R.id.menu_update:
                            Intent intent = new Intent(context, EditKaryawanActivity.class);
                            intent.putExtra("data",data);
                            context.startActivity(intent);
                            break;
                        case R.id.menu_delete:
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle(null);
                            alertDialog.setMessage("Apakah anda yakin untuk hapus karyawan ini?");
                            alertDialog.setCancelable(true);
                            alertDialog.setPositiveButton("Hapus", (dialog, which) -> apiServices.deleteDataUser(data.getNik()).enqueue(new Callback<ResponMessage>() {
                                @Override
                                public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                                    if (response.isSuccessful()){
                                        assert response.body() != null;
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                        fragment.onResume();
                                    }else{
                                        Toast.makeText(context, "gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                                    t.printStackTrace();
                                }
                            }));
                            alertDialog.setNegativeButton("Batal", (dialog, which) -> dialog.cancel());
                            alertDialog.show();
                            break;
                    }
                    return false;
                });
                popupMenu.inflate(R.menu.adapter_karyawan_menu);
                if (!prefManager.getLevel().equals(AppData.ADMIN))
                    popupMenu.getMenu().findItem(R.id.menu_delete).setVisible(false);
                try{
                    Field field = popupMenu.getClass().getDeclaredField("popup_karyawan");
                    field.setAccessible(true);
                    MenuPopupHelper popupHelper = (MenuPopupHelper) field.get(popupMenu);
                    assert popupHelper != null;
                    popupHelper.setForceShowIcon(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                popupMenu.show();
            });
        }catch (Exception e){
            e.printStackTrace();
            holder.setIsRecyclable(false);
        }
    }

    @Override
    public int getItemCount() {
        return (userFilteredList==null)?0:userFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()){
                    userFilteredList = userList;
                }else {
                    List<User> filteredList = new ArrayList<>();
                    for (User row : userList)
                        if (row.getNama().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row);
                    userFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = userFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                userFilteredList = (List<User>) results.values;
                notifyDataSetChanged();

            }
        };
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgKaryawan,btnMenu;
        TextView tvNik,tvNama,tvBagian,tvJabatan,tvDivisi,tvLevel;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNik = itemView.findViewById(R.id.tv_nik);
            tvNama =itemView.findViewById(R.id.tv_nama);
            tvBagian =itemView.findViewById(R.id.tv_bagian);
            tvJabatan = itemView.findViewById(R.id.tv_jabatan);
            tvDivisi = itemView.findViewById(R.id.tv_divisi);
            tvLevel = itemView.findViewById(R.id.tv_level);
            imgKaryawan = itemView.findViewById(R.id.img_karyawan);
            btnMenu = itemView.findViewById(R.id.btnMenu);
        }
    }
}
