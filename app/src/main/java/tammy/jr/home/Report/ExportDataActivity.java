package tammy.jr.home.Report;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.DownloadHelpers;

public class ExportDataActivity extends AppCompatActivity{
    final Calendar caldenderStart = Calendar.getInstance();
    private static final int WRITE_REQUEST_CODE = 300;
    private static final String mysqldateformat = "yyyy-MM-dd";
    final Calendar calenderEnd = Calendar.getInstance();
    private EditText edtTglMulai,edtTglSelesai;
    private Button btnExport;
    private Context context;
    private Boolean isValid = false;
    private DownloadHelpers downloadHelpers;
    private PrefManager prefManager;
    private int requestCode = 12;
    private int grantResults[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_data);
        context = this;
        initComponent();
    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        downloadHelpers = new DownloadHelpers(context,prefManager.getNIK());
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Export Report PDF");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        edtTglMulai = findViewById(R.id.edt_tgl_mulai);
        edtTglSelesai = findViewById(R.id.edt_tgl_selesai);
        btnExport = findViewById(R.id.btn_export);
        final DatePickerDialog.OnDateSetListener dateStart = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                caldenderStart.set(Calendar.YEAR, year);
                caldenderStart.set(Calendar.MONTH, monthOfYear);
                caldenderStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                    isValid =false;
                    Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
                }else{
                    isValid = true;
                    updateLabel(caldenderStart,edtTglMulai);
                }
            }
        };
        final DatePickerDialog.OnDateSetListener dateEnd = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calenderEnd.set(Calendar.YEAR, year);
                calenderEnd.set(Calendar.MONTH, monthOfYear);
                calenderEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                    isValid =false;
                    Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
                }else{
                    isValid = true;
                    updateLabel(calenderEnd,edtTglSelesai);
                }
            }
        };
        edtTglMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context,dateStart,caldenderStart.get(Calendar.YEAR),caldenderStart.get(Calendar.MONTH),caldenderStart.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        edtTglSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context,dateEnd,calenderEnd.get(Calendar.YEAR),calenderEnd.get(Calendar.MONTH),calenderEnd.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(mysqldateformat);
                if (isValid){
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(AppData.URL_EXPORT_PDF+"nik="+prefManager.getNIK()+"&start="+sdf.format(caldenderStart.getTime())+"&end="+sdf.format(calenderEnd.getTime())));
                    startActivity(i);
//                    if (downloadHelpers.checkSDCardMount()){
//                        if (checkPermision()){
//                            downloadHelpers.downloadPDF(,);
//                        }
//                    }else{
//                        Toast.makeText(context,
//                                "SD Card tidak ditemukan", Toast.LENGTH_LONG).show();
//                    }
                }else{
                    Toast.makeText(context, "tanggal tidak sesuai", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateLabel(Calendar calender,EditText edt) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        edt.setText(sdf.format(calender.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkPermision() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            //if you dont have required permissions ask for it (only required for API 23+)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
            onRequestPermissionsResult(requestCode, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, grantResults);
        }else{
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCodei, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCodei == requestCode) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("permission", "granted");
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(mysqldateformat);
                downloadHelpers.downloadPDF(sdf.format(caldenderStart.getTime()), sdf.format(calenderEnd.getTime()));
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.uujm
                Toast.makeText(context, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();

                //app cannot function without this permission for now so close it...
                onDestroy();
            }
        }
    }
}
