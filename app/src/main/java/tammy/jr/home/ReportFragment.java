package tammy.jr.home;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.adapter.AdapterListReport;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Report;
import tammy.jr.home.Report.FilterReportActivity;
import tammy.jr.home.Report.TambahReportActivity;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {
    private SwipeRefreshLayout swip;
    private RecyclerView rvReport;
    private RecyclerView.Adapter mAdapter;
    private ImageView imgNotFound;
    private Button btnAdd,btnFilter;
    private String startDate,endDate;
    private PrefManager prefManager;
    private Context context;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private View rootView;

    private AlertDialog.Builder dialog;
    private View dialogView;

    private static final int REPORT_CODE = 1001;
    private static final int ADD_CODE = 1002;
    public ReportFragment() {
        // Required empty public constructor
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_report, container, false);
        initComponent();
        return rootView;
    }

    @SuppressLint({"RestrictedApi", "InflateParams"})
    private void initComponent() {
        prefManager = new PrefManager(context);
        btnAdd = rootView.findViewById(R.id.btn_add);
        btnFilter = rootView.findViewById(R.id.btn_filter);
        rvReport = rootView.findViewById(R.id.rv_report);
        swip = rootView.findViewById(R.id.swip);
        imgNotFound = rootView.findViewById(R.id.img_notfound);

        rvReport.setHasFixedSize(true);
        rvReport.setLayoutManager(new LinearLayoutManager(context));
        if (prefManager.getLevel().equals(AppData.ADMIN) || prefManager.getLevel().equals(AppData.LEADER)){
            btnAdd.setVisibility(View.GONE);
        }else{
            btnAdd.setVisibility(View.VISIBLE);
            btnAdd.setOnClickListener(v -> startActivityForResult(new Intent(context, TambahReportActivity.class),ADD_CODE));
        }

        swip.setOnRefreshListener(() -> {
            onAttach(context);
            startDate = null;
            endDate = null;
            loadData();
        });

        btnFilter.setOnClickListener(v -> startActivityForResult(new Intent(context, FilterReportActivity.class),REPORT_CODE));
    }

    @SuppressLint("InflateParams")
    public void dialogForm(String id_report) {
        dialog = new AlertDialog.Builder(context);
        dialogView = getLayoutInflater().inflate(R.layout.popup_konfirmasi_report,null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        Button btnApproved = dialogView.findViewById(R.id.btn_approved);
        Button btnNotApproved = dialogView.findViewById(R.id.btn_not_approved);
        AlertDialog alertDialog = dialog.create();

        btnApproved.setOnClickListener(v -> {
            postKonfirmasi(id_report,"1");
            alertDialog.dismiss();
            loadData();
        });
        btnNotApproved.setOnClickListener(v -> {
            postKonfirmasi(id_report,"0");
            alertDialog.dismiss();
            loadData();
        });
        alertDialog.show();
    }

    public void postKonfirmasi(String id_report, String approval) {
        apiServices.konfirmasiReport(
                id_report,
                approval,
                prefManager.getID()
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void loadData() {
        swip.setRefreshing(true);
        String nik = "";
        if (prefManager.getLevel().equals(AppData.KARYAWAN)){
            nik = prefManager.getNIK();
        }
        apiServices.getAllReport(nik,startDate,endDate).enqueue(new Callback<List<Report>>() {
            @Override
            public void onResponse(@NotNull Call<List<Report>> call, @NotNull Response<List<Report>> response) {
                if (response.isSuccessful()){
                    try{
                        mAdapter = new AdapterListReport(context,response.body(),ReportFragment.this);
                        rvReport.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        rvReport.post(() -> swip.setRefreshing(false));
                        rvReport.setVisibility(View.VISIBLE);
                        imgNotFound.setVisibility(View.GONE);
                    }catch (Exception e){
                        e.printStackTrace();
                        imgNotFound.setVisibility(View.VISIBLE);
                        rvReport.setVisibility(View.GONE);
                        Toast.makeText(context, "terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    imgNotFound.setVisibility(View.VISIBLE);
                    rvReport.setVisibility(View.GONE);
                    swip.setRefreshing(false);
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Report>> call, @NotNull Throwable t) {
                t.printStackTrace();
                imgNotFound.setVisibility(View.VISIBLE);
                rvReport.setVisibility(View.GONE);
                swip.setRefreshing(false);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REPORT_CODE){
            if (resultCode ==RESULT_OK){
                assert data != null;
                startDate = data.getStringExtra("startDate");
                endDate = data.getStringExtra("endDate");
                loadData();
            }
        }else if (requestCode==ADD_CODE){
            loadData();
        }else if (requestCode==123){
            loadData();
        }
    }
}
