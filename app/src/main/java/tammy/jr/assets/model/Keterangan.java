package tammy.jr.assets.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Keterangan implements Serializable {
    @SerializedName("id_keterangan")
    private String idKeterangan;
    @SerializedName("keterangan")
    private String keterangan;

    public String getIdKeterangan() {
        return idKeterangan;
    }

    public void setIdKeterangan(String idKeterangan) {
        this.idKeterangan = idKeterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
