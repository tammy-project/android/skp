package tammy.jr.home.cuti;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SingleSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;

public class TambahCutiActivity extends AppCompatActivity {
    private Context context;
    Calendar calendarStart = Calendar.getInstance();
    Calendar calendarFinish = Calendar.getInstance();
    EditText edtTglMulai,edtTglSelesai,edtNamaKaryawan,edtKeperluan;
    SingleSpinnerSearch spJenisCuti,spDokumenCuti;
    private ProgressDialog loading;
    private PrefManager prefManager;
    private ApiServices apiServices= ApiUtils.getApiServices();
    private String jenisCuti,dokumenCuti;
    Button btnSimpan;
    List<String> arrJenisCuti,arrDokumenCuti;
    List<KeyPairBoolData> listJenisCuti,listDokumenCuti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_cuti);
        context = this;
        initComponent();
        prepareSpinnerJenisCuti();
        prepareSpinnerDokumenCuti();
        listener();

    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Cuti");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        edtTglMulai = findViewById(R.id.edt_tgl_mulai);
        edtTglSelesai=findViewById(R.id.edt_tgl_selesai);
        edtNamaKaryawan=findViewById(R.id.edt_nama_karyawan);
        edtKeperluan=findViewById(R.id.edt_keperluan);
        btnSimpan = findViewById(R.id.btn_simpan);
        spJenisCuti = findViewById(R.id.sp_jenis_cuti);
        spDokumenCuti = findViewById(R.id.sp_dokumen_cuti);
    }

    private void prepareSpinnerJenisCuti() {
        arrJenisCuti = Arrays.asList(getResources().getStringArray(R.array.jenis_cuti));

        listJenisCuti = new ArrayList<>();
        for(int i = 0; i< arrJenisCuti.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i+1);
            h.setName(arrJenisCuti.get(i));
            h.setSelected(false);
            listJenisCuti.add(h);
        }

    }

    private void prepareSpinnerDokumenCuti() {
        arrDokumenCuti = Arrays.asList(getResources().getStringArray(R.array.dokumen_cuti));

        listDokumenCuti = new ArrayList<>();
        for(int i = 0; i< arrDokumenCuti.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i+1);
            h.setName(arrDokumenCuti.get(i));
            h.setSelected(false);
            listDokumenCuti.add(h);
        }
    }

    private void listener() {
        spJenisCuti.setItems(listJenisCuti, -1, list -> {
            jenisCuti = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isSelected()){
                    jenisCuti = list.get(i).getName();
                    break;
                }
            }
        });
        spDokumenCuti.setItems(listDokumenCuti, -1, list -> {
            dokumenCuti = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isSelected()){
                    dokumenCuti = list.get(i).getName();
                    break;
                }
            }
        });

        //datepicker
        final DatePickerDialog.OnDateSetListener dateStart = (view, year, monthOfYear, dayOfMonth) -> {
            calendarStart.set(Calendar.YEAR, year);
            calendarStart.set(Calendar.MONTH, monthOfYear);
            calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calendarFinish.getTime().getTime()<calendarStart.getTime().getTime()){
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            }else{
                updateLabel(calendarStart,edtTglMulai);
            }
        };
        final DatePickerDialog.OnDateSetListener dateEnd = (view, year, monthOfYear, dayOfMonth) -> {
            calendarFinish.set(Calendar.YEAR, year);
            calendarFinish.set(Calendar.MONTH, monthOfYear);
            calendarFinish.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calendarFinish.getTime().getTime()<calendarFinish.getTime().getTime()){
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            }else{
                updateLabel(calendarFinish,edtTglSelesai);
            }
        };
        edtTglMulai.setOnClickListener(v -> new DatePickerDialog(context,dateStart,calendarStart.get(Calendar.YEAR),calendarStart.get(Calendar.MONTH),calendarStart.get(Calendar.DAY_OF_MONTH)).show());
        edtTglSelesai.setOnClickListener(v -> new DatePickerDialog(context,dateEnd,calendarFinish.get(Calendar.YEAR),calendarFinish.get(Calendar.MONTH),calendarFinish.get(Calendar.DAY_OF_MONTH)).show());

        btnSimpan.setOnClickListener(v -> {
            if (edtTglMulai.getText().toString().isEmpty()){
                Toast.makeText(context, "Tanggal mulai harap dipilih", Toast.LENGTH_SHORT).show();
            }else if (edtTglSelesai.getText().toString().isEmpty()){
                Toast.makeText(context, "Tanggal selesai harap dipilih", Toast.LENGTH_SHORT).show();
            }else if (edtNamaKaryawan.getText().toString().isEmpty()){
                Toast.makeText(context, "Nama karyawan harus diisi", Toast.LENGTH_SHORT).show();
            }else if (jenisCuti.isEmpty()){
                Toast.makeText(context, "Jenis cuti harus dipilih", Toast.LENGTH_SHORT).show();
            }else if (dokumenCuti.isEmpty()){
                Toast.makeText(context, "Dokumen cuti harus dipilih", Toast.LENGTH_SHORT).show();
            } else if (edtKeperluan.getText().toString().isEmpty()){
                Toast.makeText(context, "Keperluan harus diisi", Toast.LENGTH_SHORT).show();
            }else{
                postDataProcess();
            }
        });
    }

    private void postDataProcess() {
        //convert
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }else sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.addDataCuti(
                sdf.format(calendarStart.getTime()),
                sdf.format(calendarFinish.getTime()),
                edtNamaKaryawan.getText().toString(),
                edtKeperluan.getText().toString(),
                jenisCuti,
                dokumenCuti,
                prefManager.getID()
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLabel(Calendar calendar, EditText editText) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }
        editText.setText(Objects.requireNonNull(sdf).format(calendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}