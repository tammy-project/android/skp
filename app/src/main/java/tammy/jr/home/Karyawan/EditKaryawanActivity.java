package tammy.jr.home.Karyawan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.User;

public class EditKaryawanActivity extends AppCompatActivity {
    private TextInputLayout lytNik,lytNama,lytPassowrd,lytRepassword;
    private EditText edtNik,edtNama,edtPassowrd,edtRepassword;
    private Spinner spLevel,spBagian,spJabatan,spDivisi,spDepartemen;
    String[] arrLevel,arrBagian,arrJabatan,arrDivisi,arrDept;
    private Button btnSimpan;
    private Context context;
    private ProgressDialog loading;
    private ImageView imgProfil;
    private Intent intentData;
    private PrefManager prefManager;

    private File fileImage;

    public static final int PICK_FROM_GALLERY = 1001;

    private ApiServices apiServices = ApiUtils.getApiServices();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_karyawan);
        context = this;
        initComponent();
        loadData();
    }

    private void loadData() {
        User user = (User) intentData.getSerializableExtra("data");
        assert user != null;
        if (!TextUtils.isEmpty(user.getFotoUrl())){
            Picasso.get()
                    .load(AppData.URL_FOTO_KARYAWAN+user.getFotoUrl())
                    .fit()
                    .error(R.drawable.ic_noimage_profile)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imgProfil);
        }
        edtNik.setText(user.getNik());
        edtNama.setText(user.getNama());
        //level
        int i =0;
        while (i<arrLevel.length) {
            if (arrLevel[i].equals(user.getLevel())){
                spLevel.setSelection(i);
                break;
            }
            i++;
        }
        //bagian
        i = 0;
        while (i<arrBagian.length) {
            if (arrBagian[i].equals(user.getBagian())){
                spBagian.setSelection(i);
                break;
            }
            i++;
        }
        //jabatan
        i =0;
        while (i<arrJabatan.length) {
            if (arrJabatan[i].equals(user.getJabatan())){
                spJabatan.setSelection(i);
                break;
            }
            i++;
        }
        //divisi
        i =0;
        while (i<arrDivisi.length) {
            if (arrDivisi[i].equals(user.getDivisi())){
                spDivisi.setSelection(i);
                break;
            }
            i++;
        }
        //dept
        i =0;
        while (i<arrDept.length) {
            if (arrDept[i].equals(user.getDepartemen())){
                spDepartemen.setSelection(i);
                break;
            }
            i++;
        }



        if (user.getLevel().equals(AppData.ADMIN)){
            spLevel.setSelection(1);
        }else if (user.getLevel().equals(AppData.KARYAWAN)){
            spLevel.setSelection(2);
        }
    }

    private void initComponent() {
        //init toolbar
        intentData = getIntent();
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Karyawan");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        prefManager = new PrefManager(context);
        //init view
        lytNik = findViewById(R.id.lyt_nik);
        lytNama = findViewById(R.id.lyt_nama);
        spLevel = findViewById(R.id.sp_level);
        spBagian = findViewById(R.id.sp_bagian);
        spDivisi = findViewById(R.id.sp_divisi);
        spJabatan = findViewById(R.id.sp_jabatan);
        spDepartemen =findViewById(R.id.sp_departemen);
        lytPassowrd = findViewById(R.id.lyt_password);
        lytRepassword = findViewById(R.id.lyt_repassword);
        edtNik = findViewById(R.id.edt_nik);
        edtNama = findViewById(R.id.edt_nama);
        edtPassowrd = findViewById(R.id.edt_password);
        edtRepassword = findViewById(R.id.edt_repassword);
        btnSimpan = findViewById(R.id.btn_simpan);
        imgProfil = findViewById(R.id.img_profil);

        //init array
        arrLevel = getResources().getStringArray(R.array.level);
        arrBagian = getResources().getStringArray(R.array.bagian);
        arrJabatan = getResources().getStringArray(R.array.jabatan);
        arrDivisi = getResources().getStringArray(R.array.divisi);
        arrDept = getResources().getStringArray(R.array.departemen);

        //hidden form
        if (!prefManager.getLevel().equals(AppData.ADMIN)){
            lytPassowrd.setVisibility(View.GONE);
            lytRepassword.setVisibility(View.GONE);
        }


        //listener
        imgProfil.setOnClickListener(v -> checkPermisionGetFile());
        btnSimpan.setOnClickListener(v -> {
            if ((!edtPassowrd.getText().toString().isEmpty()) && (!edtRepassword.getText().toString().equals(edtPassowrd.getText().toString()))){
                lytRepassword.requestFocus();
                lytRepassword.setError("Password tidak sama");
            }else{
                updateData();
            }
        });
    }

    private void updateData() {
        String nik="",nama="",bagian="",jabatan="",divisi="",dept="",password="",level="";
        if (!edtNik.getText().toString().isEmpty()){
            nik = edtNik.getText().toString();
        }
        if (!edtNama.getText().toString().isEmpty()){
            nama = edtNama.getText().toString();
        }
        if (!spLevel.getSelectedItem().toString().isEmpty()){
            level = spLevel.getSelectedItem().toString();
        }
        if (!spBagian.getSelectedItem().toString().isEmpty()){
            bagian = spBagian.getSelectedItem().toString();
        }
        if (!spJabatan.getSelectedItem().toString().isEmpty()){
            jabatan = spJabatan.getSelectedItem().toString();
        }
        if (!spDivisi.getSelectedItem().toString().isEmpty()){
            divisi = spDivisi.getSelectedItem().toString();
        }
        if (!spDepartemen.getSelectedItem().toString().isEmpty()){
            dept = spDepartemen.getSelectedItem().toString();
        }
        if (!edtPassowrd.getText().toString().isEmpty()){
            password = edtPassowrd.getText().toString();
        }
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        MultipartBody.Part body = null;
        if (fileImage!=null){
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileImage);
            body = MultipartBody.Part.createFormData("image_data", fileImage.getName(), requestFile);
        }
        RequestBody reqNik = MultipartBody.create(MediaType.parse("multipart/form-data"), nik);
        RequestBody reqNama = MultipartBody.create(MediaType.parse("multipart/form-data"), nama);
        RequestBody reqLevel = MultipartBody.create(MediaType.parse("multipart/form-data"), level);
        RequestBody reqBagian = MultipartBody.create(MediaType.parse("multipart/form-data"), bagian);
        RequestBody reqJabatan = MultipartBody.create(MediaType.parse("multipart/form-data"), jabatan);
        RequestBody reqDivisi = MultipartBody.create(MediaType.parse("multipart/form-data"), divisi);
        RequestBody reqDept = MultipartBody.create(MediaType.parse("multipart/form-data"), dept);
        RequestBody reqPass = MultipartBody.create(MediaType.parse("multipart/form-data"), password);
        apiServices.updateDataUser(
                reqNik,
                reqNama,
                reqLevel,
                reqBagian,
                reqJabatan,
                reqDivisi,
                reqDept,
                reqPass,
                body
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void checkPermisionGetFile() {
        try{
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            }else{
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "PILIH FOTO"), PICK_FROM_GALLERY);
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Terjadi kesalahan saat request permission", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, "PILIH FOTO"), PICK_FROM_GALLERY);
                } else {
                    Toast.makeText(context, "Harap mengijinkan aplikasi mengakses galeri", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_FROM_GALLERY&&resultCode==RESULT_OK && data!=null){
            android.net.Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            android.database.Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            fileImage = new File(filePath);
            imgProfil.setImageURI(selectedImage);
        }else{
            return;
        }
    }
}
