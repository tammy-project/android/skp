package tammy.jr.assets.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Cuti implements Serializable {
    @SerializedName("id_cuti")
    private String idCuti;
    @SerializedName("nama_karywn")
    private String namaKaryawan;
    @SerializedName("tgl_mulai")
    private String tglMulai;
    @SerializedName("tgl_selesai")
    private String tglSelesai;
    @SerializedName("keperluan")
    private String keperluan;
    @SerializedName("tipe_cuti")
    private String tipe_cuti;
    @SerializedName("dok_cuti")
    private String dokCuti;
    @SerializedName("status")
    private String status;
    @SerializedName("leader_id")
    private String leader_id;
    @SerializedName("nama_leader")
    private String namaLeader;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("nama_karyawan")
    private String namaUser;
    @SerializedName("alasan_tolak")
    private String alasanTolak;

    public String getAlasanTolak() {
        return alasanTolak;
    }

    public void setAlasanTolak(String alasanTolak) {
        this.alasanTolak = alasanTolak;
    }

    public String getIdCuti() {
        return idCuti;
    }

    public void setIdCuti(String idCuti) {
        this.idCuti = idCuti;
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getTglMulai() {
        return tglMulai;
    }

    public void setTglMulai(String tglMulai) {
        this.tglMulai = tglMulai;
    }

    public String getTglSelesai() {
        return tglSelesai;
    }

    public void setTglSelesai(String tglSelesai) {
        this.tglSelesai = tglSelesai;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
    }

    public String getTipe_cuti() {
        return tipe_cuti;
    }

    public void setTipe_cuti(String tipe_cuti) {
        this.tipe_cuti = tipe_cuti;
    }

    public String getDokCuti() {
        return dokCuti;
    }

    public void setDokCuti(String dokCuti) {
        this.dokCuti = dokCuti;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLeader_id() {
        return leader_id;
    }

    public void setLeader_id(String leader_id) {
        this.leader_id = leader_id;
    }

    public String getNamaLeader() {
        return namaLeader;
    }

    public void setNamaLeader(String namaLeader) {
        this.namaLeader = namaLeader;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }
}
