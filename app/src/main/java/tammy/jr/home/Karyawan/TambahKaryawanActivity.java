package tammy.jr.home.Karyawan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;

public class TambahKaryawanActivity extends AppCompatActivity {
    private TextInputLayout lytNik,lytNama,lytPassowrd,lytRepassword;
    private EditText edtNik,edtNama,edtPassowrd,edtRepassword;
    private Spinner spLevel,spBagian,spJabatan,spDivisi,spDepartemen;
    private Button btnSimpan;
    private Context context;
    private ProgressDialog loading;
    private ImageView imgProfil;

    private File fileImage;

    public static final int PICK_FROM_GALLERY = 1001;

    private ApiServices apiServices = ApiUtils.getApiServices();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_karyawan);
        context = this;
        initComponent();
    }

    private void initComponent() {
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Karyawan");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init view
        lytNik = findViewById(R.id.lyt_nik);
        lytNama = findViewById(R.id.lyt_nama);
        spLevel = findViewById(R.id.sp_level);
        spBagian = findViewById(R.id.sp_bagian);
        spDivisi = findViewById(R.id.sp_divisi);
        spJabatan = findViewById(R.id.sp_jabatan);
        spDepartemen =findViewById(R.id.sp_departemen);
        lytPassowrd = findViewById(R.id.lyt_password);
        lytRepassword = findViewById(R.id.lyt_repassword);
        edtNik = findViewById(R.id.edt_nik);
        edtNama = findViewById(R.id.edt_nama);
        edtPassowrd = findViewById(R.id.edt_password);
        edtRepassword = findViewById(R.id.edt_repassword);
        btnSimpan = findViewById(R.id.btn_simpan);
        imgProfil = findViewById(R.id.img_profil);

        imgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermisionGetFile();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking empty value
                if (edtNik.getText().toString().isEmpty()){
                    lytNik.requestFocus();
                    lytNik.setError("harap di isi");
                }else if (edtNama.getText().toString().isEmpty()){
                    lytNama.requestFocus();
                    lytNama.setError("harap di isi");
                }else if (edtPassowrd.getText().toString().isEmpty() || (!edtRepassword.getText().toString().equals(edtPassowrd.getText().toString()))){
                    lytRepassword.requestFocus();
                    lytRepassword.setError("Password tidak sama");
                }else{
                    simpanData();
                }
            }
        });
    }

    private void checkPermisionGetFile() {
        try{
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            }else{
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "PILIH FOTO"), PICK_FROM_GALLERY);
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Terjadi kesalahan saat request permission", Toast.LENGTH_SHORT).show();
        }

    }

    private void simpanData() {
        lytNik.setError(null);
        lytNama.setError(null);
        lytPassowrd.setError(null);
        lytRepassword.setError(null);
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        MultipartBody.Part body = null;
        if (fileImage!=null){
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileImage);
            body = MultipartBody.Part.createFormData("image_data", fileImage.getName(), requestFile);
        }
        RequestBody reqNik = MultipartBody.create(MediaType.parse("multipart/form-data"), edtNik.getText().toString());
        RequestBody reqNama = MultipartBody.create(MediaType.parse("multipart/form-data"), edtNama.getText().toString());
        RequestBody reqLevel = MultipartBody.create(MediaType.parse("multipart/form-data"), spLevel.getSelectedItem().toString());
        RequestBody reqBagian = MultipartBody.create(MediaType.parse("multipart/form-data"), spBagian.getSelectedItem().toString());
        RequestBody reqJabatan = MultipartBody.create(MediaType.parse("multipart/form-data"), spJabatan.getSelectedItem().toString());
        RequestBody reqDivisi = MultipartBody.create(MediaType.parse("multipart/form-data"), spDivisi.getSelectedItem().toString());
        RequestBody reqDepartemen = MultipartBody.create(MediaType.parse("multipart/form-data"), spDepartemen.getSelectedItem().toString());
        RequestBody reqPass = MultipartBody.create(MediaType.parse("multipart/form-data"), edtPassowrd.getText().toString());
        apiServices.addDataUser(
                reqNik,
                reqNama,
                reqLevel,
                reqBagian,
                reqJabatan,
                reqDivisi,
                reqDepartemen,
                reqPass,
                body
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, "PILIH FOTO"), PICK_FROM_GALLERY);
                } else {
                    Toast.makeText(context, "Harap mengijinkan aplikasi mengakses galeri", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_FROM_GALLERY&&resultCode==RESULT_OK && data!=null){
            android.net.Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            android.database.Cursor cursor = context.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            fileImage = new File(filePath);
            imgProfil.setImageURI(selectedImage);
        }else{
            return;
        }
    }
}
