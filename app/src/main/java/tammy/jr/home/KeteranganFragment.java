package tammy.jr.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.adapter.AdapterListKeterangan;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.model.Keterangan;
import tammy.jr.home.Keterangan.TambahKeteranganActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class KeteranganFragment extends Fragment {

    private SwipeRefreshLayout swip;
    private RecyclerView rvKeterangan;
    private RecyclerView.Adapter mAdapter;
    private Context context;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_keterangan, container, false);
        initComponent();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void initComponent() {
        FloatingActionButton btnAdd = rootView.findViewById(R.id.btn_add);
        rvKeterangan = rootView.findViewById(R.id.rv_keterangan);
        swip = rootView.findViewById(R.id.swip);
        rvKeterangan.setHasFixedSize(true);
        rvKeterangan.setLayoutManager(new LinearLayoutManager(context));

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, TambahKeteranganActivity.class));
            }
        });

        swip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onAttach(context);
                loadData();
            }
        });
    }

    private void loadData() {
        swip.setRefreshing(true);
        apiServices.getAllketerangan().enqueue(new Callback<List<Keterangan>>() {
            @Override
            public void onResponse(@NotNull Call<List<Keterangan>> call, @NotNull Response<List<Keterangan>> response) {
                if (response.isSuccessful()){
                    try{
                        mAdapter = new AdapterListKeterangan(context, response.body(), KeteranganFragment.this);
                        rvKeterangan.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        rvKeterangan.post(new Runnable() {
                            @Override
                            public void run() {
                                swip.setRefreshing(false);
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                        swip.setRefreshing(false);
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    swip.setRefreshing(false);
                    Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Keterangan>> call, @NotNull Throwable t) {
                t.printStackTrace();
                swip.setRefreshing(false);
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }
}