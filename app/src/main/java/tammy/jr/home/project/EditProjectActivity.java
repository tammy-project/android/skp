package tammy.jr.home.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Project;

public class EditProjectActivity extends AppCompatActivity {
    private Context context;
    private ProgressDialog loading;
    private EditText edtProject,edtNoOka;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private Intent intentData;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);
        context = this;
        initComponent();
        loadData();
    }

    @SuppressLint("SetTextI18n")
    private void loadData() {
        Project data = (Project) intentData.getSerializableExtra("data");
        assert data != null;
        edtProject.setText(""+data.getProject());
        edtNoOka.setText(""+data.getNoOka());
        id = data.getIdProject();
    }

    private void initComponent() {
        intentData = getIntent();
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Project");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        edtNoOka = findViewById(R.id.edt_no_oka);
        edtProject = findViewById(R.id.edt_project);
        Button btnUbah = findViewById(R.id.btn_ubah);

        btnUbah.setOnClickListener(v -> {
            if(edtNoOka.getText().toString().isEmpty()){
                edtNoOka.setError("Harap di isi");
            }else if (edtProject.getText().toString().isEmpty()){
                edtProject.setError("Harap di isi");
            }else{
                edtNoOka.setError(null);
                edtProject.setError(null);
                processUbahProject();
            }
        });
    }

    private void processUbahProject() {
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.updateDataProject(id,
                edtNoOka.getText().toString(),
                edtProject.getText().toString()).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}