package tammy.jr.home.Report;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import tammy.jr.R;

public class FilterReportActivity extends AppCompatActivity {
    final Calendar caldenderStart = Calendar.getInstance();
    final Calendar calenderEnd = Calendar.getInstance();
    private EditText edtTglMulai,edtTglSelesai;
    private Context context;
    private Boolean isValid = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_report);
        context = this;
        initComponent();
    }

    private void initComponent() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Filter Report");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        edtTglMulai = findViewById(R.id.edt_tgl_mulai);
        edtTglSelesai = findViewById(R.id.edt_tgl_selesai);
        Button btnFilter = findViewById(R.id.btn_filter);
        final DatePickerDialog.OnDateSetListener dateStart = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            caldenderStart.set(Calendar.YEAR, year);
            caldenderStart.set(Calendar.MONTH, monthOfYear);
            caldenderStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (edtTglSelesai.getText().toString().isEmpty()){
                isValid = true;
                updateLabel(caldenderStart,edtTglMulai);
            }else{
                if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                    isValid =false;
                    Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
                }else{
                    isValid = true;
                    updateLabel(caldenderStart,edtTglMulai);
                }
            }
        };
        final DatePickerDialog.OnDateSetListener dateEnd = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            calenderEnd.set(Calendar.YEAR, year);
            calenderEnd.set(Calendar.MONTH, monthOfYear);
            calenderEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (edtTglMulai.getText().toString().isEmpty()){
                isValid =false;
                Toast.makeText(context, "Harap pilih tanggal mulai terlebih dahulu", Toast.LENGTH_SHORT).show();
            }else{
                if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                    isValid =false;
                    Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
                }else{
                    isValid = true;
                    updateLabel(calenderEnd,edtTglSelesai);
                }
            }
        };
        edtTglMulai.setOnClickListener(v -> new DatePickerDialog(context,dateStart,caldenderStart.get(Calendar.YEAR),caldenderStart.get(Calendar.MONTH),caldenderStart.get(Calendar.DAY_OF_MONTH)).show());
        edtTglSelesai.setOnClickListener(v -> new DatePickerDialog(context,dateEnd,calenderEnd.get(Calendar.YEAR),calenderEnd.get(Calendar.MONTH),calenderEnd.get(Calendar.DAY_OF_MONTH)).show());
        btnFilter.setOnClickListener(v -> {
            String myFormat = "yyyy-MM-dd";
            SimpleDateFormat sdf;
            if (isValid){
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
                }else{
                    sdf = new SimpleDateFormat(myFormat, Locale.US);
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("startDate",sdf.format(caldenderStart.getTime()));
                returnIntent.putExtra("endDate",sdf.format(calenderEnd.getTime()));
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }else{
                Toast.makeText(context, "tanggal tidak sesuai", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLabel(Calendar calender,EditText edt) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }

        edt.setText(sdf.format(calender.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
