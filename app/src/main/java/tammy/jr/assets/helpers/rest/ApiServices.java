package tammy.jr.assets.helpers.rest;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import tammy.jr.assets.helpers.rest.response.ResponAuth;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Cuti;
import tammy.jr.assets.model.Keterangan;
import tammy.jr.assets.model.Project;
import tammy.jr.assets.model.Report;
import tammy.jr.assets.model.Uraian;
import tammy.jr.assets.model.User;

public interface ApiServices {
    @FormUrlEncoded
    @POST("auth")
    Call<ResponAuth> login(@Field("nik") String nik, @Field("pass") String pass);

    @FormUrlEncoded
    @POST("user/get")
    Call<ResponAuth> getUserData(@Field("id") String nik);

    @GET("user/all")
    Call<List<User>> getAllUser();

    @Multipart
    @POST("user/add")
    Call<ResponMessage> addDataUser(
            @Part("nik") RequestBody nik,
            @Part("nama") RequestBody nama,
            @Part("level") RequestBody level,
            @Part("bagian") RequestBody bagian,
            @Part("jabatan") RequestBody jabatan,
            @Part("divisi") RequestBody divisi,
            @Part("departemen") RequestBody departemen,
            @Part("password") RequestBody password,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("user/update")
    Call<ResponMessage> updateDataUser(
            @Part("nik") RequestBody nik,
            @Part("nama") RequestBody nama,
            @Part("level") RequestBody level,
            @Part("bagian") RequestBody bagian,
            @Part("jabatan") RequestBody jabatan,
            @Part("divisi") RequestBody divisi,
            @Part("departemen") RequestBody departemen,
            @Part("password") RequestBody password,
            @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("user/edit")
    Call<ResponMessage> editDataUser(
            @Field("nik") String nik,
            @Field("nama") String nama,
            @Field("level") String level,
            @Field("bagian") String bagian,
            @Field("jabatan") String jabatan,
            @Field("divisi") String divisi,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("user/delete")
    Call<ResponMessage> deleteDataUser(
            @Field("id") String nik
    );

    @FormUrlEncoded
    @POST("report/all")
    Call<List<Report>> getAllReport(
            @Field("nik") String nik,
            @Field("start") String start,
            @Field("end") String end
    );

    @FormUrlEncoded
    @POST("report/delete")
    Call<ResponMessage> deleteDataReport(@Field("id") String id);

    @FormUrlEncoded
    @POST("report/add")
    Call<ResponMessage> addDataReport(
            @Field("tgl") String tanggal,
            @Field("uraian") String uraian,
            @Field("project") String project,
            @Field("other") String other,
            @Field("startDate") String startDate,
            @Field("endDate") String endDate,
            @Field("nik") String nik,
            @Field("ket") String ket
    );

    @FormUrlEncoded
    @POST("report/update")
    Call<ResponMessage> updateDataReport(
            @Field("id") String id,
            @Field("tgl") String tanggal,
            @Field("uraian") String uraian,
            @Field("project") String project,
            @Field("other") String other,
            @Field("startDate") String startDate,
            @Field("endDate") String endDate,
            @Field("nik") String nik,
            @Field("ket") String ket
    );

    @FormUrlEncoded
    @POST("user/updatepassword")
    Call<ResponMessage> updatePassword(@Field("nik") String nik, @Field("pass_baru") String passbaru, @Field("pass_lama") String passlama);

    @GET("project/all")
    Call<List<Project>> getAllProject();

    @FormUrlEncoded
    @POST("project/add")
    Call<ResponMessage> addDataProject(
            @Field("no_oka") String no_oka,
            @Field("project") String project);

    @FormUrlEncoded
    @POST("project/update")
    Call<ResponMessage> updateDataProject(
            @Field("id") String id_project,
            @Field("no_oka") String no_oka,
            @Field("project") String project
    );

    @FormUrlEncoded
    @POST("project/delete")
    Call<ResponMessage> deleteDataProject(@Field("id") String id_project);

    @GET("uraian/all")
    Call<List<Uraian>> getAllUraian();

    @FormUrlEncoded
    @POST("uraian/add")
    Call<ResponMessage> addDataUraian(@Field("uraian") String uraian);

    @FormUrlEncoded
    @POST("uraian/update")
    Call<ResponMessage> updateDataUraian(
            @Field("id") String id_uraian,
            @Field("uraian") String uraian
    );

    @FormUrlEncoded
    @POST("uraian/delete")
    Call<ResponMessage> deleteDataUraian(@Field("id") String id_uraian);

    @GET("keterangan/all")
    Call<List<Keterangan>> getAllketerangan();

    @FormUrlEncoded
    @POST("keterangan/add")
    Call<ResponMessage> addDataketerangan(@Field("keterangan") String uraian);

    @FormUrlEncoded
    @POST("keterangan/update")
    Call<ResponMessage> updateDataketerangan(
            @Field("id") String id_uraian,
            @Field("keterangan") String uraian
    );

    @FormUrlEncoded
    @POST("keterangan/delete")
    Call<ResponMessage> deleteDataketerangan(@Field("id") String id_keterangan);

    @FormUrlEncoded
    @POST("report/konfirmasi")
    Call<ResponMessage> konfirmasiReport(
            @Field("id") String id_report,
            @Field("approval") String approval,//0 untuk tolak dan 1 untuk terima
            @Field("user") String id_yang_appropal
    );

    @GET("cuti/all/{id}")
    Call<List<Cuti>> getAllCuti(
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("cuti/approval")
    Call<ResponMessage> konfirmasiCuti(
            @Field("id") String id,
            @Field("status") String status,
            @Field("alasan") String alasan_tolak,
            @Field("leader_id") String leader_id
    );

    @FormUrlEncoded
    @POST("cuti/store")
    Call<ResponMessage> addDataCuti(
            @Field("tgl_mulai") String tgl_mulai,
            @Field("tgl_selesai") String tgl_selesai,
            @Field("nama_karyawan") String nama_karyawan,
            @Field("keperluan") String keperluan,
            @Field("tipe_cuti") String tipe_cuti,
            @Field("dok_cuti") String dok_cuti,
            @Field("id_user") String id_user
    );

    @FormUrlEncoded
    @POST("cuti/update")
    Call<ResponMessage> updateDataCuti(
            @Field("id") String id_cuti,
            @Field("tgl_mulai") String tgl_mulai,
            @Field("tgl_selesai") String tgl_selesai,
            @Field("nama_karyawan") String nama_karyawan,
            @Field("keperluan") String keperluan,
            @Field("tipe_cuti") String tipe_cuti,
            @Field("dok_cuti") String dok_cuti,
            @Field("id_user") String id_user
    );

    @FormUrlEncoded
    @POST("cuti/destroy")
    Call<ResponMessage> deleteDataCuti(
            @Field("id") String id_cuti
    );


}
