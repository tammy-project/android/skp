package tammy.jr.assets.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Project;
import tammy.jr.home.project.EditProjectActivity;

public class AdapterListProject extends RecyclerView.Adapter<AdapterListProject.MyViewHolder> {
    private final Context context;
    private final List<Project> projectList;
    private final Fragment fragment;
    private final ApiServices apiServices = ApiUtils.getApiServices();
    private PrefManager prefManager;

    public AdapterListProject(Context context, List<Project> projectList, Fragment fragment) {
        this.context = context;
        this.projectList = projectList;
        this.fragment = fragment;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_project, parent, false));
    }

    @SuppressLint({"SetTextI18n", "RestrictedApi", "NonConstantResourceId"})
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        try{
            final Project data = projectList.get(position);

            holder.tvProject.setText(""+data.getProject());
            if (TextUtils.isEmpty(data.getNoOka()))
                holder.tvNoOka.setText("-");
            else
                holder.tvNoOka.setText(data.getNoOka());

            if (!prefManager.getLevel().equals(AppData.ADMIN))
                holder.btnMenu.setVisibility(View.GONE);

            holder.btnMenu.setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context,holder.btnMenu);
                popupMenu.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()){
                        case R.id.menu_update:
                            Intent intent = new Intent(context, EditProjectActivity.class);
                            intent.putExtra("data",data);
                            context.startActivity(intent);
                            break;
                        case R.id.menu_delete:
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle(null);
                            alertDialog.setMessage("Apakah anda yakin untuk hapus Project ini?");
                            alertDialog.setCancelable(true);
                            alertDialog.setPositiveButton("Hapus", (dialog, which) -> apiServices.deleteDataProject(data.getIdProject()).enqueue(new Callback<ResponMessage>() {
                                @Override
                                public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                                    if (response.isSuccessful()){
                                        assert response.body() != null;
                                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                        fragment.onResume();
                                    }else{
                                        Toast.makeText(context, "gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                                    t.printStackTrace();
                                }
                            }));
                            alertDialog.setNegativeButton("Batal", (dialog, which) -> dialog.cancel());
                            alertDialog.show();
                            break;
                    }
                    return false;
                });
                popupMenu.inflate(R.menu.adapter_karyawan_menu);
                try{
                    Field field = popupMenu.getClass().getDeclaredField("popup_project");
                    field.setAccessible(true);
                    MenuPopupHelper popupHelper = (MenuPopupHelper) field.get(popupMenu);
                    assert popupHelper != null;
                    popupHelper.setForceShowIcon(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                popupMenu.show();
            });
        }catch (Exception e){
            e.printStackTrace();
            holder.setIsRecyclable(false);
        }
    }

    @Override
    public int getItemCount() {
        return (projectList==null)?0:projectList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView btnMenu;
        TextView tvProject,tvNoOka;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProject = itemView.findViewById(R.id.tv_project);
            tvNoOka = itemView.findViewById(R.id.tv_no_oka);
            btnMenu = itemView.findViewById(R.id.btnMenu);
        }
    }
}
