package tammy.jr.home;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        TextView tvLevel = rootView.findViewById(R.id.tv_level);
        PrefManager prefManager = new PrefManager(context);

        switch (prefManager.getLevel()) {
            case AppData.KARYAWAN:
                tvLevel.setText("Anda Adalah Karyawan");
                break;
            case AppData.ADMIN:
                tvLevel.setText("Anda Adalah Admin");
                break;
            case AppData.LEADER:
                tvLevel.setText("Anda Adalah Leader");
                break;
            case AppData.PPC:
                tvLevel.setText("Anda Adalah PPC");
                break;
            case AppData.HRGA:
                tvLevel.setText("Anda Adalah HR");
                break;
        }
        return rootView;
    }

}
