package tammy.jr.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.adapter.AdapterListKayawan;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.model.User;
import tammy.jr.home.Karyawan.TambahKaryawanActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class KaryawanFragment extends Fragment {
    private SwipeRefreshLayout swip;
    private RecyclerView rvKaryawan;
    private FloatingActionButton btnAdd;
    private AdapterListKayawan mAdapter;
    private SearchView searchView;
    private Context context;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private View rootView;
    public KaryawanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_karyawan, container, false);
        initComponent();
        return rootView;
    }

    private void initComponent() {
        searchView = rootView.findViewById(R.id.searchView);
        btnAdd = rootView.findViewById(R.id.btn_add);
        rvKaryawan = rootView.findViewById(R.id.rv_karyawan);
        swip = rootView.findViewById(R.id.swip);
        rvKaryawan.setHasFixedSize(true);
        rvKaryawan.setLayoutManager(new LinearLayoutManager(context));
        rvKaryawan.setItemAnimator(new DefaultItemAnimator());

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });


        btnAdd.setOnClickListener(v -> startActivity(new Intent(context, TambahKaryawanActivity.class)));

        swip.setOnRefreshListener(() -> {
            onAttach(context);
            loadData();
        });
    }

    private void loadData() {
        swip.setRefreshing(true);
        apiServices.getAllUser().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(@NotNull Call<List<User>> call, @NotNull Response<List<User>> response) {
                if (response.isSuccessful()){
                    try {
                        mAdapter = new AdapterListKayawan(context,response.body(),KaryawanFragment.this);
                        rvKaryawan.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        rvKaryawan.post(new Runnable() {
                            @Override
                            public void run() {
                                swip.setRefreshing(false);
                            }
                        });
                    }catch (Exception e){
                        swip.setRefreshing(false);
                        e.printStackTrace();
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    swip.setRefreshing(false);
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<User>> call, @NotNull Throwable t) {
                t.printStackTrace();
                swip.setRefreshing(false);
            }
        });
    }

}
