package tammy.jr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.model.User;
import tammy.jr.home.CutiFragment;
import tammy.jr.home.DashboardFragment;
import tammy.jr.home.KaryawanFragment;
import tammy.jr.home.KeteranganFragment;
import tammy.jr.home.ProfilFragment;
import tammy.jr.home.ProjectFragment;
import tammy.jr.home.Report.ExportDataActivity;
import tammy.jr.home.ReportFragment;
import tammy.jr.home.UraianFragment;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private NavigationView navigationView;
    private PrefManager prefManager;
    private Context context;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        initComponent();
    }

    @SuppressLint("SetTextI18n")
    private void initComponent() {
        prefManager = new PrefManager(context);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        this.navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
//        View headerView = navigationView.getHeaderView(0);
        this.navigationView.setNavigationItemSelectedListener(this);
        hideMenu();
        TextView versionName = findViewById(R.id.version);
        versionName.setText("Version : " + BuildConfig.VERSION_NAME);
    }

    private void hideMenu() {
        User userData = prefManager.getUserLogin();
        Menu navMenu = this.navigationView.getMenu();
        switch (userData.getLevel()) {
            case AppData.ADMIN:
                navMenu.findItem(R.id.menu_karyawan).setVisible(true);
                navMenu.findItem(R.id.menu_project).setVisible(true);
                navMenu.findItem(R.id.menu_uraian).setVisible(true);
                navMenu.findItem(R.id.menu_keterangan).setVisible(false);
                navMenu.findItem(R.id.menu_cuti).setVisible(true);
                break;
            case AppData.PPC:
                navMenu.findItem(R.id.menu_karyawan).setVisible(false);
                navMenu.findItem(R.id.menu_project).setVisible(true);
                navMenu.findItem(R.id.menu_uraian).setVisible(true);
                navMenu.findItem(R.id.menu_keterangan).setVisible(false);
                navMenu.findItem(R.id.menu_report).setVisible(false);
                navMenu.findItem(R.id.menu_cuti).setVisible(false);
                break;
            case AppData.HRGA:
                navMenu.findItem(R.id.menu_karyawan).setVisible(true);
                navMenu.findItem(R.id.menu_project).setVisible(false);
                navMenu.findItem(R.id.menu_uraian).setVisible(false);
                navMenu.findItem(R.id.menu_keterangan).setVisible(false);
                navMenu.findItem(R.id.menu_report).setVisible(false);
                navMenu.findItem(R.id.menu_cuti).setVisible(true);
                navMenu.findItem(R.id.menu_cuti).setTitle("View Pengajuan Cuti");
                break;
            case AppData.LEADER:
                navMenu.findItem(R.id.menu_karyawan).setVisible(false);
                navMenu.findItem(R.id.menu_project).setVisible(false);
                navMenu.findItem(R.id.menu_uraian).setVisible(false);
                navMenu.findItem(R.id.menu_keterangan).setVisible(false);
                navMenu.findItem(R.id.menu_report).setVisible(true);
                navMenu.findItem(R.id.menu_cuti).setVisible(false);
                break;
            default:
                navMenu.findItem(R.id.menu_karyawan).setVisible(false);
                navMenu.findItem(R.id.menu_project).setVisible(false);
                navMenu.findItem(R.id.menu_uraian).setVisible(false);
                navMenu.findItem(R.id.menu_keterangan).setVisible(false);
                navMenu.findItem(R.id.menu_cuti).setVisible(true);
                break;
        }
        loadFragment(new DashboardFragment());
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    System.exit(0);
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(context, "Klik Back 2x untuk keluar", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_download) {
            startActivity(new Intent(context, ExportDataActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.menu_dashboard) {
            loadFragment(new DashboardFragment());
        } else if (id == R.id.menu_profil) {
            loadFragment(new ProfilFragment());
        } else if (id == R.id.menu_karyawan) {
            loadFragment(new KaryawanFragment());
        } else if (id == R.id.menu_project) {
            loadFragment(new ProjectFragment());
        } else if (id == R.id.menu_uraian) {
            loadFragment(new UraianFragment());
        } else if (id == R.id.menu_keterangan) {
            loadFragment(new KeteranganFragment());
        } else if (id == R.id.menu_report) {
            loadFragment(new ReportFragment());
        } else if (id == R.id.menu_cuti){
            loadFragment(new CutiFragment());
        }else if (id == R.id.menu_logout) {
            prefManager.clearAllData();
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadFragment(Fragment fragment) {
        MenuItem downloadMenu = toolbar.getMenu().findItem(R.id.menu_download);
        if (downloadMenu != null) {
            downloadMenu.setVisible(false);
            if (fragment.getClass().getSimpleName().equals(DashboardFragment.class.getSimpleName())) {
                toolbar.setTitle("Dashboard");
            } else if (fragment.getClass().getSimpleName().equals(KaryawanFragment.class.getSimpleName())) {
                toolbar.setTitle("Data Karyawan");
            } else if (fragment.getClass().getSimpleName().equals(ProfilFragment.class.getSimpleName())) {
                toolbar.setTitle("Profil Sistem");
            } else if (fragment.getClass().getSimpleName().equals(ProjectFragment.class.getSimpleName())) {
                toolbar.setTitle("Data Project");
            } else if (fragment.getClass().getSimpleName().equals(UraianFragment.class.getSimpleName())) {
                toolbar.setTitle("Data Uraian");
            } else if (fragment.getClass().getSimpleName().equals(KeteranganFragment.class.getSimpleName())) {
                toolbar.setTitle("Data Keterangan");
            } else if (fragment.getClass().getSimpleName().equals(ReportFragment.class.getSimpleName())) {
                toolbar.setTitle("Report Pekerjaan Karyawan");
                if (!prefManager.getLevel().equals("admin")){
                    downloadMenu.setVisible(true);
                }
            } else if (fragment.getClass().getSimpleName().equals(CutiFragment.class.getSimpleName())){
                if (prefManager.getLevel().equals(AppData.KARYAWAN)){
                    toolbar.setTitle("Pengajuan Cuti");
                }else{
                    toolbar.setTitle("View Pengajuan Cuti");
                }
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fm.beginTransaction()
                .replace(R.id.frame, fragment, "HOME")
                .commit();
    }
}
