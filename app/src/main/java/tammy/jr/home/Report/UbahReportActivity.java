package tammy.jr.home.Report;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Project;
import tammy.jr.assets.model.Report;
import tammy.jr.assets.model.Uraian;

public class UbahReportActivity extends AppCompatActivity {
    private Context context;
    final Calendar caldenderdate = Calendar.getInstance();
    final Calendar caldenderStart = Calendar.getInstance();
    final Calendar calenderEnd = Calendar.getInstance();
    private Boolean clickCalenderDate = false;
    private Boolean clickCalenderStart = false;
    private Boolean clickCalenderEnd = false;
    private EditText edtDate, edtOther, edtStartDate, edtEndDate,edtKeterangan;
    private String idReport;
    private Intent intentData;
    private Button btnSimpan;
    private ProgressDialog loading;
    private PrefManager prefManager;
    private ApiServices apiServices = ApiUtils.getApiServices();
    MultiSpinnerSearch spUraian, spProject, spKet;
    private String uraian, project;
    private String uraianId, projectId;
//    List<String> arrUraian, arrProject, arrKet;
    List<KeyPairBoolData> listUraian;
    List<KeyPairBoolData> listProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_report);
        context = this;
        initComponent();
        loadData();
    }

    private void loadData() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yy");
        intentData = getIntent();
        Report data = (Report) intentData.getSerializableExtra("data");
        //calender date
        if (data != null) {
            try {
                idReport = data.getId();
                try {
                    caldenderdate.setTime(Objects.requireNonNull(sdf.parse(data.getHari_tgl())));
                    clickCalenderDate = true;
                    updateLabel(caldenderdate, edtDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    caldenderStart.setTime(Objects.requireNonNull(sdf.parse(data.getWaktu())));
                    clickCalenderStart = true;
                    updateLabel(caldenderStart, edtStartDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    calenderEnd.setTime(Objects.requireNonNull(sdf.parse(data.getTgl_selesai())));
                    clickCalenderEnd = true;
                    updateLabel(calenderEnd, edtEndDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                edtOther.setText(data.getLain());
                edtKeterangan.setText(data.getKuan());
//                setup list
                //uraian/kegiatan
                prepareDataUraian(data);
                prepareDataProject(data);

//                setup spinner

            } catch (Exception e) {
                e.printStackTrace();
                finish();
                Toast.makeText(context, "Gagal mengambil data", Toast.LENGTH_SHORT).show();
            }
        } else {
            finish();
            Toast.makeText(context, "Gagal mengambil data", Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareDataProject(Report data) {
        apiServices.getAllProject().enqueue(new Callback<List<Project>>() {
            @Override
            public void onResponse(@NotNull Call<List<Project>> call, @NotNull Response<List<Project>> response) {
                if (response.isSuccessful()){
                    List<KeyPairBoolData> listProject = new ArrayList<>();
                    List<String> splitProject;
                    if (data.getUraian()!=null){
                        splitProject = new ArrayList<>(Arrays.asList(data.getProject().split(", ")));
                    }else{
                        splitProject = new ArrayList<>();
                    }

                    assert response.body() != null;
                    for(int i = 0; i< response.body().size(); i++) {
                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Integer.parseInt(response.body().get(i).getIdProject()));
                        h.setName(response.body().get(i).getProject());
                        if (splitProject.size() != 0) {
                            h.setSelected(splitProject.contains(response.body().get(i).getProject()));
                        } else {
                            h.setSelected(false);
                        }
                        listProject.add(h);
                    }

                    spProject.setItems(listProject, -1, list -> {
                        project = "";
                        projectId = "";
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).isSelected()) {
                                if (i==list.size()-1){
                                    project += list.get(i).getName();
                                    projectId += list.get(i).getId();
                                }else{
                                    project += list.get(i).getName()+", ";
                                    projectId += list.get(i).getId()+", ";
                                }
                            }
                        }
                    });

                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Project>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void prepareDataUraian(Report data) {
        apiServices.getAllUraian().enqueue(new Callback<List<Uraian>>() {
            @Override
            public void onResponse(@NotNull Call<List<Uraian>> call, @NotNull Response<List<Uraian>> response) {
                if (response.isSuccessful()){
                    List<KeyPairBoolData> listUraian = new ArrayList<>();
                    List<String> splitUraian;
                    if (data.getUraian()!=null){
                        splitUraian = new ArrayList<>(Arrays.asList(data.getUraian().split(", ")));
                    }else{
                        splitUraian = new ArrayList<>();
                    }
                    assert response.body() != null;
                    for(int i = 0; i< response.body().size(); i++) {
                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Integer.parseInt(response.body().get(i).getIdUraian()));
                        h.setName(response.body().get(i).getUraian());
                        if (splitUraian.size() != 0) {
                            h.setSelected(splitUraian.contains(response.body().get(i).getUraian()));
                        } else {
                            h.setSelected(false);
                        }
                        listUraian.add(h);
                    }

                    spUraian.setItems(listUraian, -1, list -> {
                        uraian = "";
                        uraianId = "";
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).isSelected()){
                                if (i==list.size()-1){
                                    uraian += list.get(i).getName();
                                    uraianId += list.get(i).getId();
                                }else{
                                    uraian += list.get(i).getName() + ", ";
                                    uraianId += list.get(i).getId()+", ";
                                }
                            }
                        }

                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Uraian>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Report");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        edtDate = findViewById(R.id.edt_date);
        spUraian = findViewById(R.id.sp_uraian);
        spProject = findViewById(R.id.sp_project);
        edtOther = findViewById(R.id.edt_other);
        edtStartDate = findViewById(R.id.edt_start_date);
        edtEndDate = findViewById(R.id.edt_end_date);
        btnSimpan = findViewById(R.id.btn_simpan);
        edtKeterangan = findViewById(R.id.edt_keterangan);


        final DatePickerDialog.OnDateSetListener dateStart = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            caldenderStart.set(Calendar.YEAR, year);
            caldenderStart.set(Calendar.MONTH, monthOfYear);
            caldenderStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calenderEnd.getTime().getTime() < caldenderStart.getTime().getTime()) {
                clickCalenderStart = false;
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            } else {
                clickCalenderStart = true;
                updateLabel(caldenderStart, edtStartDate);
            }
        };
        final DatePickerDialog.OnDateSetListener dateEnd = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            calenderEnd.set(Calendar.YEAR, year);
            calenderEnd.set(Calendar.MONTH, monthOfYear);
            calenderEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calenderEnd.getTime().getTime() < caldenderStart.getTime().getTime()) {
                clickCalenderEnd = false;
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            } else {
                clickCalenderEnd = true;
                updateLabel(calenderEnd, edtEndDate);
            }
        };
        final DatePickerDialog.OnDateSetListener dateInput = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            caldenderdate.set(Calendar.YEAR, year);
            caldenderdate.set(Calendar.MONTH, monthOfYear);
            caldenderdate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            clickCalenderDate = true;
            updateLabel(caldenderdate, edtDate);
        };
        edtDate.setOnClickListener(v -> new DatePickerDialog(context, dateInput, caldenderdate.get(Calendar.YEAR), caldenderdate.get(Calendar.MONTH), caldenderdate.get(Calendar.DAY_OF_MONTH)).show());
        edtStartDate.setOnClickListener(v -> new DatePickerDialog(context, dateStart, caldenderStart.get(Calendar.YEAR), caldenderStart.get(Calendar.MONTH), caldenderStart.get(Calendar.DAY_OF_MONTH)).show());
        edtEndDate.setOnClickListener(v -> new DatePickerDialog(context, dateEnd, calenderEnd.get(Calendar.YEAR), calenderEnd.get(Calendar.MONTH), calenderEnd.get(Calendar.DAY_OF_MONTH)).show());
        btnSimpan.setOnClickListener(v -> processubahReport());
    }

    private void processubahReport() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        } else {
            sdf = new SimpleDateFormat(myFormat, Locale.US);
        }
        String stringDate = "", stringDateStart = "", stringDateEnd = "";
        if (clickCalenderDate) {
            stringDate = sdf.format(caldenderdate.getTime());
        }
        if (clickCalenderStart) {
            stringDateStart = sdf.format(caldenderStart.getTime());
        }
        if (clickCalenderEnd) {
            stringDateEnd = sdf.format(calenderEnd.getTime());
        }
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.updateDataReport(
                idReport,
                stringDate,
                uraianId,
                projectId,
                edtOther.getText().toString(),
                stringDateStart,
                stringDateEnd,
                prefManager.getNIK(),
                edtKeterangan.getText().toString()
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()) {
                    try {
                        if (loading != null && loading.isShowing()) {
                            loading.dismiss();
                        }
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()) {
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (loading != null && loading.isShowing()) {
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (loading != null && loading.isShowing()) {
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading != null && loading.isShowing()) {
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateLabel(Calendar calendar, EditText edt) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }

        edt.setText(sdf.format(calendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
