package tammy.jr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponAuth;
import tammy.jr.assets.model.User;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LoginActivity extends AppCompatActivity {
    private Context context;
    private ImageView logoReka;
    private Button btnLogin;
    private EditText edtNik,edtPass;
    private ProgressBar loadingProgressBar;
    private RelativeLayout rootView, afterAnimationView;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private PrefManager prefManager;
    private ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        initComponent();
        checkLogin();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNik.getText().toString().isEmpty()){
                    edtNik.requestFocus();
                    edtNik.setError("Harap di isi");
                }else if (edtPass.getText().toString().isEmpty()){
                    edtPass.requestFocus();
                    edtPass.setError("Harap di isi");
                }else{
                    loginProcess();
                }
            }
        });
    }

    private void checkLogin() {
        User dataUser = prefManager.getUserLogin();
        if (!TextUtils.isEmpty(dataUser.getNik())){
            new Handler().postDelayed(() -> {
                Intent intent = new Intent(context,HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }, 1000);
        }else{
            showLoginForm();
        }
    }

    private void showLoginForm() {
        new Handler().postDelayed(() -> new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                loadingProgressBar.setVisibility(GONE);
                rootView.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                logoReka.setImageResource(R.drawable.logoreka);
                startAnimation();
            }

            @Override
            public void onFinish() {

            }
        }.start(),1000);
    }

    private void loginProcess() {
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.login(edtNik.getText().toString(),edtPass.getText().toString()).enqueue(new Callback<ResponAuth>() {
            @Override
            public void onResponse(@NotNull Call<ResponAuth> call, @NotNull Response<ResponAuth> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    if (response.body().getStatus()){
                        prefManager.setDataUser(response.body().getResult());
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                    }else{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Terjadi kesalahan saat mengambil data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponAuth> call, @NotNull Throwable t) {
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
            }
        });
    }

    private void startAnimation() {
        ViewPropertyAnimator viewPropertyAnimator = logoReka.animate();
        viewPropertyAnimator.x(50f);
        viewPropertyAnimator.y(100f);
        viewPropertyAnimator.setDuration(1000);
        viewPropertyAnimator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                afterAnimationView.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        logoReka = findViewById(R.id.img_logo_reka);
        loadingProgressBar = findViewById(R.id.loadingProgressBar);
        rootView = findViewById(R.id.rootView);
        afterAnimationView = findViewById(R.id.afterAnimationView);
        btnLogin = findViewById(R.id.btn_login);
        edtNik = findViewById(R.id.edt_nik);
        edtPass = findViewById(R.id.edt_pass);
    }
}
