package tammy.jr.home.Report;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Project;
import tammy.jr.assets.model.Uraian;

public class TambahReportActivity extends AppCompatActivity{
    private Context context;
    final Calendar caldenderdate = Calendar.getInstance();
    final Calendar caldenderStart = Calendar.getInstance();
    final Calendar calenderEnd = Calendar.getInstance();
    private EditText edtDate,edtOther,edtStartDate,edtEndDate,edtKeterangan;
    private ProgressDialog loading;
    private PrefManager prefManager;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private String uraian,uraianId,project,projectId;
    MultiSpinnerSearch spUraian ,spProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_report);
        context = this;
        initComponent();
    }

    private void initComponent() {
        prefManager= new PrefManager(context);
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Report");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        edtDate =findViewById(R.id.edt_date);
        spUraian = findViewById(R.id.sp_uraian);
        spProject = findViewById(R.id.sp_project);
        edtOther = findViewById(R.id.edt_other);
        edtStartDate = findViewById(R.id.edt_start_date);
        edtEndDate = findViewById(R.id.edt_end_date);
        edtKeterangan = findViewById(R.id.edt_keterangan);
        Button btnSimpan = findViewById(R.id.btn_simpan);

        //get array
        prepareDataUraian();
        prepareDataProject();

        //datepicker
        final DatePickerDialog.OnDateSetListener dateStart = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            caldenderStart.set(Calendar.YEAR, year);
            caldenderStart.set(Calendar.MONTH, monthOfYear);
            caldenderStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            }else{
                updateLabel(caldenderStart,edtStartDate);
            }
        };
        final DatePickerDialog.OnDateSetListener dateEnd = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            calenderEnd.set(Calendar.YEAR, year);
            calenderEnd.set(Calendar.MONTH, monthOfYear);
            calenderEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                Toast.makeText(context, "Tanggal mulai dan tanggal selesai tidak valid", Toast.LENGTH_SHORT).show();
            }else{
                updateLabel(calenderEnd,edtEndDate);
            }
        };
        final DatePickerDialog.OnDateSetListener dateInput = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            caldenderdate.set(Calendar.YEAR, year);
            caldenderdate.set(Calendar.MONTH, monthOfYear);
            caldenderdate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(caldenderdate,edtDate);
        };
        edtDate.setOnClickListener(v -> new DatePickerDialog(context, dateInput, caldenderdate.get(Calendar.YEAR), caldenderdate.get(Calendar.MONTH), caldenderdate.get(Calendar.DAY_OF_MONTH)).show());
        edtStartDate.setOnClickListener(v -> new DatePickerDialog(context,dateStart,caldenderStart.get(Calendar.YEAR),caldenderStart.get(Calendar.MONTH),caldenderStart.get(Calendar.DAY_OF_MONTH)).show());
        edtEndDate.setOnClickListener(v -> new DatePickerDialog(context,dateEnd,calenderEnd.get(Calendar.YEAR),calenderEnd.get(Calendar.MONTH),calenderEnd.get(Calendar.DAY_OF_MONTH)).show());
        btnSimpan.setOnClickListener(v -> {
            if (edtDate.getText().toString().isEmpty()){
                Toast.makeText(context, "Tanggal harap di isi", Toast.LENGTH_SHORT).show();
            } else if (uraianId.isEmpty()){
                Toast.makeText(context, "Uraian harap di pilih", Toast.LENGTH_SHORT).show();
            } else if (projectId.isEmpty()){
                Toast.makeText(context, "Project harap di pilih", Toast.LENGTH_SHORT).show();
            }else if (edtOther.getText().toString().isEmpty()){
                Toast.makeText(context, "Lain-lain harap di isi", Toast.LENGTH_SHORT).show();
            }else if (edtStartDate.getText().toString().isEmpty()){
                Toast.makeText(context, "Tanggal mulai harap di isi", Toast.LENGTH_SHORT).show();
            }else if (edtEndDate.getText().toString().isEmpty()){
                Toast.makeText(context, "Tanggal selesai harap di isi", Toast.LENGTH_SHORT).show();
            }else if (calenderEnd.getTime().getTime()<caldenderStart.getTime().getTime()){
                Toast.makeText(context, "Tanggal mulai dan selesai tidak sesuai", Toast.LENGTH_SHORT).show();
            }else if(edtKeterangan.getText().toString().isEmpty()){
                Toast.makeText(context, "Keterangan pekerjaan harap di pilih", Toast.LENGTH_SHORT).show();
            }else{
                processTambahReport();
            }
        });
    }

    private void prepareDataProject() {
        apiServices.getAllProject().enqueue(new Callback<List<Project>>() {
            @Override
            public void onResponse(@NotNull Call<List<Project>> call, @NotNull Response<List<Project>> response) {
                if (response.isSuccessful()){
                    List<KeyPairBoolData> listProject = new ArrayList<>();
                    assert response.body() != null;
                    for(int i = 0; i< response.body().size(); i++) {
                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Integer.parseInt(response.body().get(i).getIdProject()));
                        h.setName(response.body().get(i).getProject());
                        h.setSelected(false);
                        listProject.add(h);
                    }

                    spProject.setItems(listProject, -1, list -> {
                        project = "";
                        projectId = "";
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).isSelected()) {
                                if (i==list.size()-1){
                                    project += list.get(i).getName();
                                    projectId += list.get(i).getId();
                                }else{
                                    project += list.get(i).getName()+", ";
                                    projectId += list.get(i).getId()+", ";
                                }
                            }
                        }
                    });

                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Project>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void prepareDataUraian() {
        apiServices.getAllUraian().enqueue(new Callback<List<Uraian>>() {
            @Override
            public void onResponse(@NotNull Call<List<Uraian>> call, @NotNull Response<List<Uraian>> response) {
                if (response.isSuccessful()){
                    List<KeyPairBoolData> listUraian = new ArrayList<>();
                    assert response.body() != null;
                    for(int i = 0; i< response.body().size(); i++) {
                        KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(Integer.parseInt(response.body().get(i).getIdUraian()));
                        h.setName(response.body().get(i).getUraian());
                        h.setSelected(false);
                        listUraian.add(h);
                    }

                    spUraian.setItems(listUraian, -1, list -> {
                        uraian = "";
                        uraianId = "";
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).isSelected()){
                                if (i==list.size()-1){
                                    uraian += list.get(i).getName();
                                    uraianId += list.get(i).getId();
                                }else{
                                    uraian += list.get(i).getName() + ", ";
                                    uraianId += list.get(i).getId()+", ";
                                }
                            }
                        }

                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Uraian>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void processTambahReport() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }else sdf = new SimpleDateFormat(myFormat, Locale.US);
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.addDataReport(
                sdf.format(caldenderdate.getTime()),
                spUraian.getSelectedItem().toString(),
                projectId,
                edtOther.getText().toString(),
                sdf.format(caldenderStart.getTime()),
                sdf.format(calenderEnd.getTime()),
                prefManager.getNIK(),
                edtKeterangan.getText().toString()
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLabel(Calendar calendar, EditText edt) {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            sdf = new SimpleDateFormat(myFormat, Locale.forLanguageTag("ID"));
        }

        edt.setText(Objects.requireNonNull(sdf).format(calendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
