package tammy.jr.assets.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Report implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("tahun")
    private String tahun;
    @SerializedName("nik")
    private String nik;
    @SerializedName("kegiatan")
    private String kegiatan;
    @SerializedName("panel")
    private String panel;
    @SerializedName("kuan")
    private String kuan;
    @SerializedName("s_kuan")
    private String s_kuan;
    @SerializedName("lain")
    private String lain;
    @SerializedName("waktu")
    private String waktu;
    @SerializedName("waktu_val")
    private String waktu_val;
    @SerializedName("hari_tgl")
    private String hari_tgl;
    @SerializedName("hari_tgl_val")
    private String hari_tgl_val;
    @SerializedName("tgl_selesai")
    private String tgl_selesai;
    @SerializedName("tgl_selesai_val")
    private String tgl_selesai_val;
    @SerializedName("ket")
    private String keterangan;
    @SerializedName("nama_karyawan")
    private String namaKaryawan;
    @SerializedName("nama_leader")
    private String namaLeader;
    @SerializedName("approval")
    private String approval;
    @SerializedName("approval_oleh")
    private String approvalOleh;
    @SerializedName("project")
    private String project;
    @SerializedName("uraian")
    private String uraian;

    public String getWaktu_val() {
        return waktu_val;
    }

    public void setWaktu_val(String waktu_val) {
        this.waktu_val = waktu_val;
    }

    public String getTgl_selesai_val() {
        return tgl_selesai_val;
    }

    public void setTgl_selesai_val(String tgl_selesai_val) {
        this.tgl_selesai_val = tgl_selesai_val;
    }

    public String getHari_tgl_val() {
        return hari_tgl_val;
    }

    public void setHari_tgl_val(String hari_tgl_val) {
        this.hari_tgl_val = hari_tgl_val;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getNamaLeader() {
        return namaLeader;
    }

    public void setNamaLeader(String namaLeader) {
        this.namaLeader = namaLeader;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getApprovalOleh() {
        return approvalOleh;
    }

    public void setApprovalOleh(String approvalOleh) {
        this.approvalOleh = approvalOleh;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }

    public String getPanel() {
        return panel;
    }

    public void setPanel(String panel) {
        this.panel = panel;
    }

    public String getKuan() {
        return kuan;
    }

    public void setKuan(String kuan) {
        this.kuan = kuan;
    }

    public String getS_kuan() {
        return s_kuan;
    }

    public void setS_kuan(String s_kuan) {
        this.s_kuan = s_kuan;
    }

    public String getLain() {
        return lain;
    }

    public void setLain(String lain) {
        this.lain = lain;
    }

    public String getHari_tgl() {
        return hari_tgl;
    }

    public void setHari_tgl(String hari_tgl) {
        this.hari_tgl = hari_tgl;
    }

    public String getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(String tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }
}
