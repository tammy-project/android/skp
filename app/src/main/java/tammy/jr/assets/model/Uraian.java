package tammy.jr.assets.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Uraian implements Serializable {
    @SerializedName("id_uraian")
    private String idUraian;
    @SerializedName("uraian")
    private String uraian;

    public String getIdUraian() {
        return idUraian;
    }

    public void setIdUraian(String idUraian) {
        this.idUraian = idUraian;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }
}
