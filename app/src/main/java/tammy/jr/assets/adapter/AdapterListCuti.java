package tammy.jr.assets.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.model.Cuti;
import tammy.jr.home.CutiFragment;
import tammy.jr.home.cuti.EditCutiActivity;

public class AdapterListCuti extends RecyclerView.Adapter<AdapterListCuti.MyViewHolder> implements Filterable {
    private Context context;
    private List<Cuti> cutiList, cutiFilteredList;
    private CutiFragment fragment;
    //    private ApiServices apiServices = ApiUtils.getApiServices();
    PrefManager prefManager;

    public AdapterListCuti(Context context, List<Cuti> cutiList, CutiFragment fragment) {
        this.context = context;
        this.cutiList = cutiList;
        this.fragment = fragment;
        this.cutiFilteredList = cutiList;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_cuti, parent, false));
    }

    @SuppressLint({"SetTextI18n", "RestrictedApi"})
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            Cuti data = cutiFilteredList.get(position);
            holder.tvTglCuti.setText(": " + data.getTglMulai() + " - " + data.getTglSelesai());
            holder.tvJenisCuti.setText(": " + data.getTipe_cuti());
            holder.tvNamakaryawan.setText(": " + data.getNamaKaryawan());
            holder.tvDokumenCuti.setText(": " + data.getDokCuti());
            holder.tvKeperluan.setText(": " + data.getKeperluan());
            switch (data.getStatus()) {
                case "1":
                    holder.tvStatus.setText("Menunggu Approval");
                    holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.menunggu));
                    holder.btnMenu.setVisibility(View.VISIBLE);
                    break;
                case "2":
                    holder.tvStatus.setText("Disetujui");
                    holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.diterima));
                    holder.btnMenu.setVisibility(View.GONE);
                    break;
                case "3":
                    holder.tvStatus.setText("Ditolak \n\"" + data.getAlasanTolak() + "\"");
                    holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.ditolak));
                    if (prefManager.getLevel().equals(AppData.KARYAWAN)) {
                        holder.btnMenu.setVisibility(View.VISIBLE);
                    } else {
                        holder.btnMenu.setVisibility(View.GONE);
                    }
                    break;
                case "4":
                    holder.tvStatus.setText("Pending");
                    holder.tvStatus.setBackgroundColor(context.getResources().getColor(R.color.pending));
                    if (prefManager.getLevel().equals(AppData.KARYAWAN)) {
                        holder.btnMenu.setVisibility(View.VISIBLE);
                    } else {
                        holder.btnMenu.setVisibility(View.GONE);
                    }
                    break;
            }

            holder.btnApproved.setOnClickListener(v -> {
                fragment.postKonfirmasi(data.getIdCuti(), null, "2");
                fragment.onResume();
            });
            holder.btnNotApproved.setOnClickListener(v -> {
                holder.layoutButton.setVisibility(View.GONE);
                holder.layoutAlasan.setVisibility(View.VISIBLE);
                holder.btnKirim.setOnClickListener(v1 -> {
                    if (holder.edtAlasan.getText().toString().isEmpty()) {
                        holder.edtAlasan.setError("Harap di isi");
                        holder.edtAlasan.requestFocus();
                    } else {
                        holder.edtAlasan.setError(null);
                        fragment.postKonfirmasi(data.getIdCuti(), holder.edtAlasan.getText().toString(), "3");
                        fragment.onResume();
                    }
                });
            });
            holder.btnPending.setOnClickListener(v -> {
                fragment.postKonfirmasi(data.getIdCuti(), null, "4");
                fragment.onResume();
            });


            //setup menu
            holder.btnMenu.setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context, holder.btnMenu);
                popupMenu.setOnMenuItemClickListener(item -> {
                    if (item.getItemId() == R.id.menu_ubah) {
                        Intent intent = new Intent(context, EditCutiActivity.class);
                        intent.putExtra("data", data);
                        fragment.startActivityForResult(intent, 123);
                    }
                    return false;
                });
                popupMenu.inflate(R.menu.adapter_cuti);
                switch (prefManager.getLevel()) {
                    case AppData.LEADER:
                        holder.lytKonfirmasi.setVisibility(View.GONE);
                        break;
                    case AppData.HRGA:
                        popupMenu.getMenu().findItem(R.id.menu_ubah).setVisible(false);
                        holder.lytKonfirmasi.setVisibility(View.VISIBLE);
                        break;
                    default:
                        popupMenu.getMenu().findItem(R.id.menu_ubah).setVisible(true);
                        holder.lytKonfirmasi.setVisibility(View.GONE);
                }

                try {
                    Field field = popupMenu.getClass().getDeclaredField("popup_cuti");
                    field.setAccessible(true);
                    MenuPopupHelper popupHelper = (MenuPopupHelper) field.get(popupMenu);
                    assert popupHelper != null;
                    popupHelper.setForceShowIcon(true);
                } catch (Exception ignored) {
                }

                popupMenu.show();
            });

            if (AppData.HRGA.equals(prefManager.getLevel())) {
                if (data.getStatus().equals("1"))
                    holder.lytKonfirmasi.setVisibility(View.VISIBLE);
                else
                    holder.lytKonfirmasi.setVisibility(View.GONE);
            } else {
                holder.lytKonfirmasi.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.setIsRecyclable(false);
        }
    }

    @Override
    public int getItemCount() {
        return (cutiFilteredList == null) ? 0 : cutiFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    cutiFilteredList = cutiList;
                } else {
                    List<Cuti> filteredList = new ArrayList<>();
                    for (Cuti row : cutiList)
                        if (row.getNamaKaryawan().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row);
                    cutiFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = cutiFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                cutiFilteredList = (List<Cuti>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTglCuti, tvNamakaryawan, tvJenisCuti, tvDokumenCuti, tvKeperluan, tvStatus;
        ImageView btnMenu;
        LinearLayout lytKonfirmasi, layoutButton, layoutAlasan;
        Button btnApproved, btnNotApproved, btnPending, btnKirim;
        EditText edtAlasan;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTglCuti = itemView.findViewById(R.id.tv_tgl_cuti);
            tvNamakaryawan = itemView.findViewById(R.id.tv_nama_karyawan);
            tvJenisCuti = itemView.findViewById(R.id.tv_jenis_cuti);
            tvDokumenCuti = itemView.findViewById(R.id.tv_dokumen_cuti);
            tvKeperluan = itemView.findViewById(R.id.tv_keperluan);
            tvStatus = itemView.findViewById(R.id.tv_status);
            btnMenu = itemView.findViewById(R.id.btnMenu);
            lytKonfirmasi = itemView.findViewById(R.id.layout_konfirmasi);
            btnApproved = itemView.findViewById(R.id.btn_approved);
            btnNotApproved = itemView.findViewById(R.id.btn_not_approved);
            btnPending = itemView.findViewById(R.id.btn_pending);
            layoutButton = itemView.findViewById(R.id.layout_button);
            layoutAlasan = itemView.findViewById(R.id.layout_alasan);
            edtAlasan = itemView.findViewById(R.id.edt_alasan_tolak);
            btnKirim = itemView.findViewById(R.id.btn_kirim);
        }
    }
}
