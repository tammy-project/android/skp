package tammy.jr.assets.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Uraian;
import tammy.jr.home.Uraian.EditUraianActivity;

public class AdapterListUraian extends RecyclerView.Adapter<AdapterListUraian.MyViewHolder> {
    private final Context context;
    private final List<Uraian> uraianList;
    private final Fragment fragment;
    private final ApiServices apiServices = ApiUtils.getApiServices();
    private final PrefManager prefManager;

    public AdapterListUraian(Context context, List<Uraian> uraianList, Fragment fragment) {
        this.context = context;
        this.uraianList = uraianList;
        this.fragment = fragment;
        prefManager = new PrefManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_uraian, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        try{
            final Uraian data = uraianList.get(position);
            holder.tvUraian.setText(""+data.getUraian());
            if (!prefManager.getLevel().equals(AppData.ADMIN))
                holder.btnMenu.setVisibility(View.GONE);
            holder.btnMenu.setOnClickListener(v -> {
                PopupMenu popupMenu = new PopupMenu(context,holder.btnMenu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.menu_update:
                                Intent intent = new Intent(context, EditUraianActivity.class);
                                intent.putExtra("data",data);
                                context.startActivity(intent);
                                break;
                            case R.id.menu_delete:
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle(null);
                                alertDialog.setMessage("Apakah anda yakin untuk hapus Uraian ini?");
                                alertDialog.setCancelable(true);
                                alertDialog.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        apiServices.deleteDataUraian(data.getIdUraian()).enqueue(new Callback<ResponMessage>() {
                                            @Override
                                            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                                                if (response.isSuccessful()){
                                                    assert response.body() != null;
                                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                                    fragment.onResume();
                                                }else{
                                                    Toast.makeText(context, "gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                                                t.printStackTrace();
                                            }
                                        });
                                    }
                                });
                                alertDialog.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                alertDialog.show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.adapter_karyawan_menu);
                try{
                    Field field = popupMenu.getClass().getDeclaredField("popup_uraian");
                    field.setAccessible(true);
                    MenuPopupHelper popupHelper = (MenuPopupHelper) field.get(popupMenu);
                    assert popupHelper != null;
                    popupHelper.setForceShowIcon(true);
                }catch (Exception e){
                    e.printStackTrace();
                }
                popupMenu.show();
            });
        }catch (Exception e){
            e.printStackTrace();
            holder.setIsRecyclable(false);
        }
    }

    @Override
    public int getItemCount() {
        return (uraianList==null)?0:uraianList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView btnMenu;
        TextView tvUraian;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUraian = itemView.findViewById(R.id.tv_uraian);
            btnMenu = itemView.findViewById(R.id.btnMenu);
        }
    }
}
