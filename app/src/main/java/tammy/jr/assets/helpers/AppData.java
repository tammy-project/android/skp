package tammy.jr.assets.helpers;

public class AppData {
    public static final String LINK = "ptrekaindo.co.id/jr/";
    public static final String URL = "https://"+LINK;

    public static final String URL_API = URL+"api/";
    public static final String URL_FOTO_KARYAWAN = URL+"assets/img/karyawan/";
    public static final String URL_EXPORT_PDF = URL_API+"report/export_pdf?";
    //level
    public static final String ADMIN = "admin";
    public static final String KARYAWAN = "karyawan";
    public static final String LEADER = "leader";
    public static final String PPC = "PPC";
    public static final String HRGA = "HRGA";
}
