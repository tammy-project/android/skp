package tammy.jr.home.Profil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.User;

public class UbahProfilActivity extends AppCompatActivity {
    private TextInputLayout lytNik,lytNama,lytBagian,lytJabatan,lytDivisi,lytPassowrd,lytRepassword;
    private EditText edtNik,edtNama,edtBagian,edtJabatan,edtDivisi,edtPassowrd,edtRepassword;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private ProgressDialog loading;
    private Spinner spLevel;
    private Button btnUbah;
    private Intent intentData;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_profil);
        context = this;
        initComponent();
    }

    private void initComponent() {
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Profil");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //init view
        lytNik = findViewById(R.id.lyt_nik);
        lytNama = findViewById(R.id.lyt_nama);
        spLevel = findViewById(R.id.sp_level);
        lytBagian = findViewById(R.id.lyt_bagian);
        lytJabatan = findViewById(R.id.lyt_jabatan);
        lytDivisi = findViewById(R.id.lyt_divisi);
        lytPassowrd = findViewById(R.id.lyt_password);
        lytRepassword = findViewById(R.id.lyt_repassword);
        edtNik = findViewById(R.id.edt_nik);
        edtNama = findViewById(R.id.edt_nama);
        edtBagian = findViewById(R.id.edt_bagian);
        edtJabatan = findViewById(R.id.edt_jabatan);
        edtDivisi = findViewById(R.id.edt_divisi);
        edtPassowrd = findViewById(R.id.edt_password);
        edtRepassword = findViewById(R.id.edt_repassword);
        btnUbah = findViewById(R.id.btn_simpan);

        intentData = getIntent();

        loadData();
        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checking empty value
                if (edtNik.getText().toString().isEmpty()){
                    lytNik.requestFocus();
                    lytNik.setError("harap di isi");
                }else if (edtNama.getText().toString().isEmpty()){
                    lytNama.requestFocus();
                    lytNama.setError("harap di isi");
                }else if (spLevel.getSelectedItem().toString().isEmpty()){
                    Toast.makeText(UbahProfilActivity.this, "Harus di pilih", Toast.LENGTH_SHORT).show();
                }else if (edtBagian.getText().toString().isEmpty()){
                    lytBagian.requestFocus();
                    lytBagian.setError("harap di isi");
                }else if (edtJabatan.getText().toString().isEmpty()){
                    lytJabatan.requestFocus();
                    lytJabatan.setError("harap di isi");
                }else if (edtDivisi.getText().toString().isEmpty()){
                    lytDivisi.requestFocus();
                    lytDivisi.setError("harap di isi");
                }else if (!edtPassowrd.getText().toString().isEmpty() && edtRepassword.getText().toString().equals(edtRepassword.getText().toString())){
                    lytRepassword.requestFocus();
                    lytRepassword.setError("Password tidak sama");
                }else{
                    ubahProcess();
                }
            }
        });
    }

    private void ubahProcess() {
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.editDataUser(
                edtNik.getText().toString(),
                edtNama.getText().toString(),
                spLevel.getSelectedItem().toString(),
                edtBagian.getText().toString(),
                edtJabatan.getText().toString(),
                edtDivisi.getText().toString(),
                edtPassowrd.getText().toString()).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            finish();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadData() {
        User user = (User) intentData.getSerializableExtra("data");
        assert user != null;
        edtNik.setText(user.getNik());
        edtNama.setText(user.getNama());
        if (user.getLevel().equals(AppData.ADMIN)){
            spLevel.setSelection(0);
        }else if (user.getLevel().equals(AppData.KARYAWAN)){
            spLevel.setSelection(1);
        }
        edtBagian.setText(user.getBagian());
        edtJabatan.setText(user.getJabatan());
        edtDivisi.setText(user.getDivisi());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
