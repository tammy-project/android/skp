package tammy.jr.assets.helpers.rest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import tammy.jr.assets.helpers.AppData;

public class DownloadHelpers {
    private Context context;
    private Activity activity;
    private String TAG = "DOWNLOAD";
    private String nik;

    public DownloadHelpers(Context context, String nik) {
        this.context = context;
        this.nik = nik;
    }

    public void downloadPDF(String startDate, String endDate){
        new DownloadFile().execute(AppData.URL_EXPORT_PDF+"nik="+nik+"&start="+startDate+"&end="+endDate);
    }

    public boolean checkSDCardMount(){
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            return true;
        }
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadFile extends AsyncTask<String,String,String>{
        private ProgressDialog progressDialog;
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                @SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
                //Extract file name from URL
                String fileName = "laporan_skp";
                //Append timestamp to file name
                fileName = fileName+"_"+timestamp;
                //External directory path to save file
//                String folder = Environment.getExternalStorageState().toString() + "/skp/";
                String folder = "";
                if (Environment.getExternalStorageState() == null){
                    folder = Environment.getDataDirectory().getPath()+"/skp/";
                }else if (Environment.getExternalStorageState()!=null){
                    folder = Environment.getExternalStorageDirectory().getPath()+"/skp/";
                }
                File directory = new File(folder);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName+".pdf");
                byte[] data = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Error: ", Objects.requireNonNull(e.getMessage()));
            }
            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
