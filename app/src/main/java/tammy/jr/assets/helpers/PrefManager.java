package tammy.jr.assets.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import tammy.jr.assets.model.User;

public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "data_skp";
    //user
    private static final String ID = "id";
    private static final String NIK = "nik";
    private static final String NAMA = "nama";
    private static final String BAGIAN = "bagian";
    private static final String JABATAN = "jabatan";
    private static final String DIVISI = "divisi";
    private static final String FOTO = "foto";
    private static final String PASS = "pass";
    private static final String LEVEL = "level";


    @SuppressLint("CommitPrefEdits")
    public PrefManager(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setDataUser(User user) {
        editor.putString(ID, user.getId());
        editor.putString(NIK, user.getNik());
        editor.putString(NAMA, user.getNama());
        editor.putString(BAGIAN, user.getBagian());
        editor.putString(JABATAN, user.getJabatan());
        editor.putString(DIVISI, user.getDivisi());
        editor.putString(FOTO, user.getFotoUrl());
        editor.putString(PASS, user.getPass());
        editor.putString(LEVEL, user.getLevel());
        editor.commit();
    }

    public String getID() {
        return pref.getString(ID, "");
    }

    public String getNIK() {
        return pref.getString(NIK, "");
    }

    public String getNama() {
        return pref.getString(NAMA, "");
    }

    public String getLevel() {
        return pref.getString(LEVEL, "");
    }

    public User getUserLogin() {
        User tempUser = new User();
        tempUser.setNik(pref.getString(NIK, ""));
        tempUser.setNama(pref.getString(NAMA, ""));
        tempUser.setBagian(pref.getString(BAGIAN, ""));
        tempUser.setJabatan(pref.getString(JABATAN, ""));
        tempUser.setDivisi(pref.getString(DIVISI, ""));
        tempUser.setFotoUrl(pref.getString(FOTO, ""));
        tempUser.setPass(pref.getString(PASS, ""));
        tempUser.setLevel(pref.getString(LEVEL, ""));
        return tempUser;
    }

    @SuppressLint("ApplySharedPref")
    public void clearAllData() {
        editor.clear().commit();
    }
}
