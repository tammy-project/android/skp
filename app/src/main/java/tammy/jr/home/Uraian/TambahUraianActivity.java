package tammy.jr.home.Uraian;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;

public class TambahUraianActivity extends AppCompatActivity {

    private Context context;
    private ProgressDialog loading;
    private EditText edtUraian;
    private ApiServices apiServices = ApiUtils.getApiServices();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_uraian);
        context = this;
        initComponent();
    }

    private void initComponent() {
        //init toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Uraian");
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        edtUraian = findViewById(R.id.edt_uraian);
        Button btnSimpan = findViewById(R.id.btn_simpan);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUraian.getText().toString().isEmpty()){
                    edtUraian.setError("Harap di isi");
                }else{
                    edtUraian.setError(null);
                    processTambah();
                }
            }
        });
    }

    private void processTambah() {
        loading = ProgressDialog.show(this, null, "harap tunggu...", true, false);
        apiServices.addDataUraian(edtUraian.getText().toString()).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    try{
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        assert response.body() != null;
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().isStatus()){
                            onBackPressed();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if (loading!=null && loading.isShowing()){
                            loading.dismiss();
                        }
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (loading!=null && loading.isShowing()){
                        loading.dismiss();
                    }
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (loading!=null && loading.isShowing()){
                    loading.dismiss();
                }
                Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}