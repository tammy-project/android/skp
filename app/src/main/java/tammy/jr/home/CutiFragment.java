package tammy.jr.home;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tammy.jr.R;
import tammy.jr.assets.adapter.AdapterListCuti;
import tammy.jr.assets.helpers.AppData;
import tammy.jr.assets.helpers.PrefManager;
import tammy.jr.assets.helpers.rest.ApiServices;
import tammy.jr.assets.helpers.rest.ApiUtils;
import tammy.jr.assets.helpers.rest.response.ResponMessage;
import tammy.jr.assets.model.Cuti;
import tammy.jr.home.cuti.TambahCutiActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CutiFragment extends Fragment {
    private Context context;
    private RecyclerView rvCuti;
    private AdapterListCuti mAdapter;
    private FloatingActionButton btnAdd;
    private SwipeRefreshLayout swip;
    private ApiServices apiServices = ApiUtils.getApiServices();
    private View rooView;
    private SearchView searchView;

    PrefManager prefManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rooView = inflater.inflate(R.layout.fragment_cuti, container, false);
        initComponent();
        listener();
        return rooView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void listener() {
        btnAdd.setOnClickListener(v -> startActivity(new Intent(context, TambahCutiActivity.class)));

        swip.setOnRefreshListener(() -> {
            onAttach(context);
            loadData();
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    private void loadData() {
        String id_user = "";
        if (prefManager.getLevel().equals(AppData.KARYAWAN))
            id_user = prefManager.getID();
        swip.setRefreshing(true);
        apiServices.getAllCuti(id_user).enqueue(new Callback<List<Cuti>>() {
            @Override
            public void onResponse(@NotNull Call<List<Cuti>> call, @NotNull Response<List<Cuti>> response) {
                if (response.isSuccessful()){
                    try{
                        mAdapter = new AdapterListCuti(context,response.body(),CutiFragment.this);
                        rvCuti.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        rvCuti.post(() -> swip.setRefreshing(false));
                    }catch (Exception e){
                        e.printStackTrace();
                        swip.setRefreshing(false);
                        Toast.makeText(context, "Terjadi kesalahan pada aplikasi", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    swip.setRefreshing(false);
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Cuti>> call, @NotNull Throwable t) {
                t.printStackTrace();
                swip.setRefreshing(false);
                Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initComponent() {
        prefManager = new PrefManager(context);
        btnAdd = rooView.findViewById(R.id.btn_add);
        rvCuti = rooView.findViewById(R.id.rv_cuti);
        swip = rooView.findViewById(R.id.swip);
        searchView = rooView.findViewById(R.id.searchView);
        rvCuti = rooView.findViewById(R.id.rv_cuti);
        rvCuti.setHasFixedSize(true);
        rvCuti.setLayoutManager(new LinearLayoutManager(context));
        rvCuti.setItemAnimator(new DefaultItemAnimator());
        if (prefManager.getLevel().equals(AppData.KARYAWAN)){
            btnAdd.setVisibility(View.VISIBLE);
        }else{
            btnAdd.setVisibility(View.GONE);
        }
    }

    @SuppressLint("InflateParams")
    public void dialogForm(String id_cuti) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        View dialogView = getLayoutInflater().inflate(R.layout.popup_konfirmasi_cuti, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        Button btnApproved = dialogView.findViewById(R.id.btn_approved);
        Button btnNotApproved = dialogView.findViewById(R.id.btn_not_approved);
        Button btnPending = dialogView.findViewById(R.id.btn_pending);
        LinearLayout layoutButton = dialogView.findViewById(R.id.layout_button);
        LinearLayout layoutAlasan = dialogView.findViewById(R.id.layout_alasan);
        EditText edtAlasan = dialogView.findViewById(R.id.edt_alasan_tolak);
        AlertDialog alertDialog = dialog.create();
        Button btnKirim = dialogView.findViewById(R.id.btn_kirim);

        btnApproved.setOnClickListener(v -> {
            postKonfirmasi(id_cuti,null,"2");
            alertDialog.dismiss();
            onResume();
        });
        btnNotApproved.setOnClickListener(v -> {
            layoutButton.setVisibility(View.GONE);
            layoutAlasan.setVisibility(View.VISIBLE);
            btnKirim.setOnClickListener(v1 -> {
                if (edtAlasan.getText().toString().isEmpty()){
                    edtAlasan.setError("Harap di isi");
                    edtAlasan.requestFocus();
                }else{
                    edtAlasan.setError(null);
                    postKonfirmasi(id_cuti,edtAlasan.getText().toString(),"3");
                    alertDialog.dismiss();
                    onResume();
                }
            });
        });
        btnPending.setOnClickListener(v -> {
            postKonfirmasi(id_cuti,null,"4");
            alertDialog.dismiss();
            onResume();
        });
        alertDialog.show();
    }

    public void postKonfirmasi(String id_cuti, String alasan, String status) {
        apiServices.konfirmasiCuti(
                id_cuti,
                status,
                alasan,
                prefManager.getID()
        ).enqueue(new Callback<ResponMessage>() {
            @Override
            public void onResponse(@NotNull Call<ResponMessage> call, @NotNull Response<ResponMessage> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "Gagal terhubung ke server", Toast.LENGTH_SHORT).show();
                }
                onResume();
            }

            @Override
            public void onFailure(@NotNull Call<ResponMessage> call, @NotNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(context, "Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show();
            }
        });
    }
}