package tammy.jr.assets.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Project implements Serializable {
    @SerializedName("id_project")
    private String idProject;
    @SerializedName("no_oka")
    private String noOka;
    @SerializedName("project")
    private String project;

    public String getNoOka() {
        return noOka;
    }

    public void setNoOka(String noOka) {
        this.noOka = noOka;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
